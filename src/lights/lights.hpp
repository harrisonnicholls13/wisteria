#include "../wisteria.hpp"

// Type chars:
//  s = Sun


class Light{
    // Variables
    protected:
        double      col[3] = {1.0,1.0,1.0}; 
        double      lum    = 70;
    public:
        bool        point  = true;
        double      pos[3] = {0.0,0.0,0.0};
        double      ex_len = 200.0;
        bool        show   = true;
        std::string name   = "unnamed";

    // Functions
    public:
        virtual         ~Light() = default;
        virtual void    get_illum(double* out, double* p) = 0;
        virtual void    rotate(char ax, double theta, double* cor) = 0;
        virtual void    sample(double* lopos, int s) = 0;
        virtual char    get_type() = 0;
                void    set_color(int hexValue);
                double* get_color();
                void    translate(double* v);
        

};


class Sun : public Light{
    // Variables
    private:
        std::mt19937 gen;
        std::uniform_real_distribution<> dist;
    public:
        double  radius = 0.0;
        const char  type = 's';

    // Functions
    public:
                Sun(double x, double y, double z, double r, double L);
                ~Sun();
        char    get_type();
        void    get_illum(double* out, double* p);
        void    rotate(char ax, double theta, double* cor);
        void    sample(double* lopos, int s);
};

