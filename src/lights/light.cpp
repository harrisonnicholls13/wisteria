#include "../wisteria.hpp"

void Light::translate(double* v){
    vadd(pos,pos,v);
}


/**
 * Set the light source's color based on a hex value
 * @param hexValue  New color
 */
void Light::set_color(int hexValue) {

    double r = ((hexValue >> 16) & 0xFF) / 255.0;  // Extract the RR byte
    double g = ((hexValue >> 8) & 0xFF)  / 255.0;  // Extract the GG byte
    double b = ((hexValue) & 0xFF)       / 255.0;  // Extract the BB byte
    double c[3] = {r,g,b};
    for (int i = 0; i < 3; i++) {
        if ((c[i] < 0) || (c[i] > 1.0)) {
            printf(PRT_WARN"New light source color value out of range \n");
            return;
        }
    }
    vequal(col,c);
}

/**
 * Obtain the light source's color as R,G,B components from 0 to 1.
 * @returns     Color components in a length 3 vector (double)
 */
double* Light::get_color(){
    return col;
}

