#include "../wisteria.hpp"

Sun::~Sun(){
    // Destructor for sun
    return;
}

Sun::Sun(double x, double y, double z, double R, double L){
    pos[0] = x;
    pos[1] = y;
    pos[2] = z;
    lum = fabs(L);
    radius = fabs(R);

    point = (radius == 0);

    if (!point) {
        std::random_device rd;
        gen = std::mt19937(rd());
        dist = std::uniform_real_distribution<>(-1.0*radius,1.0*radius);
    }
}

void Sun::get_illum(double* out, double* p) {
    // Get vector between evaluation point and source
    double dr[3];
    vsub(dr,p,pos);

    // Get distance^2 between source and point
    double L2;
    vdot(&L2,dr,dr);

    // Not good if they intersect, as it blows up
    if (L2 <= 0){
        printf(PRT_WARN"Object intersects light source \n");
    } else {
        // Calculate scale factor and apply to colors
        double sf = lum/L2;
        out[0] = col[0] * sf;
        out[1] = col[1] * sf;
        out[2] = col[2] * sf;
    }
}


void Sun::rotate(char ax, double theta, double* cor) {
    UNUSED(cor);
    
    double** mrot = alloc2D(3,3);
    rotmat3D(mrot,ax,theta);
    vmatmul(pos,mrot,pos);
    free2D(mrot,3);
}

/**
 * Sample a random position on this light source
 * @param lopos     Result: random position on light source
 * @param s         Sample number
 */
void Sun::sample(double* lopos, int s) {

    if ( (s == 0) || point) {
        // If first sample, use true position of light source
        vequal(lopos,pos);
    } else {
        // Otherwise, pick a random position within its radius
        double per[3] = {dist(gen),dist(gen),dist(gen)};
        vadd(lopos,pos,per);
    }
}

/**
 * Return light type as char
 * @returns Type char
 */
char Sun::get_type(){
    return 's';
}
