#include "../wisteria.hpp"

Scene::Scene(std::string filepath, bool verb){
    verbose = (verb || (DEBUG>0));

    if(verbose) printf(PRT_MSG"Initalising scene... \n");

    // Initialise random number generator
    std::random_device rd;
    gen = std::mt19937(rd());
    dist = std::normal_distribution<>(0,0.1);
    for (int i = 0; i < 99; i++) sample_gaussian();    // Call the generator a few times to warm it up

    // Check file is okay
    std::fstream check_config;
    check_config.open(filepath.c_str());
    if (check_config.fail()) {
        printf(PRT_ERROR"In Scene(): Could not open config file '%s' \n",filepath.c_str());
        exit(EXIT_FAILURE);
    }
    check_config.close();

    // Read config file for this scene
    // https://github.com/jbeder/yaml-cpp/wiki/Tutorial
    if(verbose) printf(PRT_MSG"\tConfig file:      '%s' \n",filepath.c_str());
    YAML::Node config = YAML::LoadFile(filepath);

    if (config["outfile"])
        outfile = config["outfile"].as<std::string>();
    if(verbose) printf(PRT_MSG"\tOutput image file:       '%s'\n",outfile.c_str());
    

    if (config["camera_posX"] && config["camera_posY"] && config["camera_posZ"]) {
        cam_pos[0]  = config["camera_posX"].as<double>();
        cam_pos[1]  = config["camera_posY"].as<double>();
        cam_pos[2]  = config["camera_posZ"].as<double>();
        if(verbose) printf(PRT_MSG"\tCamera position:     (%g,%g,%g)\n",cam_pos[0],cam_pos[1],cam_pos[2]);
    }

    if (config["camera_width"] && config["camera_height"] && config["camera_depth"]) {
        cam_dx  = fabs(config["camera_width"].as<double>());
        cam_dy  = fabs(config["camera_height"].as<double>());
        cam_dz  = fabs(config["camera_depth"].as<double>());
    }
    if(verbose) printf(PRT_MSG"\tCamera dimensions:       (%g,%g,%g)\n",cam_dx,cam_dy,cam_dz);

    if (config["camera_lati"])
        cam_lati = config["camera_lati"].as<double>();
    if(verbose) printf(PRT_MSG"\tCamera latitude angle:   %g deg\n",cam_lati);

    if (config["camera_long"] )
        cam_long = config["camera_long"].as<double>();
    if(verbose) printf(PRT_MSG"\tCamera longitude angle:  %g deg\n",cam_long);

    if (config["num_procs"]) {
        tmax = abs(config["num_procs"].as<int>());
        tmax = std::min(tmax,int(std::thread::hardware_concurrency()-2));        
        tmax = std::max(1,tmax); 
    }
    if(verbose) printf(PRT_MSG"\tNum. sub-processes:      %d\n",tmax);

    if (config["max_ptile"]) {
        max_pct = std::min(config["max_ptile"].as<double>(),100.0);
        max_pct = std::max(max_pct,1.0);
    }
    if(verbose) printf(PRT_MSG"\tClamp upper percentile:  %g\n",max_pct);

    if (config["reflect_iters"])
        rttl = std::max(config["reflect_iters"].as<int>(),0);
    if(verbose) printf(PRT_MSG"\tNum. ray bounces:        %d\n",rttl);

    if (config["reflect_samps"])
        mcrr = std::max(config["reflect_samps"].as<int>(),0);
    if(verbose) printf(PRT_MSG"\tNum. scattering samples: %d\n",mcrr);

    if (config["shadow_samps"])
        mcsr = std::max(config["shadow_samps"].as<int>(),1);
    if(verbose) printf(PRT_MSG"\tNum. shadow samples:     %d\n",mcsr);

    if (config["gamma"])
        gamma = config["gamma"].as<double>();
    if(verbose) printf(PRT_MSG"\tGamma factor:            %g\n",gamma);

    if (config["nair"]) 
        nair = config["nair"].as<double>();
    if(verbose) printf(PRT_MSG"\tAir refractive idx:      %g\n",nair);

    if (config["max_opt_depth"]) 
        taumax = std::max(config["max_opt_depth"].as<double>(),0.0);
    if(verbose) printf(PRT_MSG"\tMaximum optical depth:   %g\n",taumax);

    if (config["attenuate_air"]) 
        amultair = std::max(config["attenuate_air"].as<double>(),0.0);
    if(verbose) printf(PRT_MSG"\tAir atten. multiplier:   %g\n",amultair);


    // Other derived parameters
    cam_rx =   int(abs(cam_dx*cam_res));
    cam_ry =   int(abs(cam_dy*cam_res));
    cam_nix =  cam_rx*cam_ry;
    cam_cs =   std::ceil((double)cam_nix/tmax);

    if(verbose) printf(PRT_MSG"\n");

    // Load entities if any exist in file
    const YAML::Node& objs = config["objects"];
    double x,y,z,L,w,h,d,r,scale,rot,rgh,rfractidx,amult; // variables that are needed for different object properties
    double trans[3];
    int col = 0xFFFFFF; // color code
    int ntype = 0;      // type of object most recently added (0->light, 1->entity)
    std::string name;
    std::string file;
    bool any_rough = false;  // Check if any materials have reflective roughness
    bool any_dist = false;   // Check if any light sources are distributed
    if (objs) {
        if(verbose) printf(PRT_MSG"Loading entities and lights... \n");
        // loop over objects
        for (YAML::const_iterator it = objs.begin(); it != objs.end(); ++it) {
            const YAML::Node& obj = *it;
            const std::string type = obj["type"].as<std::string>();

            // Handle based on type of object
            // Uses try/catch to ensure all required arguments are included in the config file
            if (type == "sun") {
                try {
                    name = obj["name"].as<std::string>();
                    x = obj["posX"].as<double>();
                    y = obj["posY"].as<double>();
                    z = obj["posZ"].as<double>();
                    r = obj["radius"].as<double>();
                    L = obj["power"].as<double>();
                    if (r>0) any_dist = true;
                } catch (...) {
                    printf(PRT_WARN"\tFailed to load Light[Sun] \n");
                    continue;
                }
                add_light(new Sun(x,y,z,r,L));
                light_last()->name = name.substr(0,NAME_LEN);
                ntype = 0;
                if(verbose) printf(PRT_MSG"\tLoaded Light[Sun] '%s' into scene at: (%g,%g,%g) \n",name.c_str(),x,y,z);

            } else if (type == "square") {
                try {
                    name = obj["name"].as<std::string>();
                    x = obj["posX"].as<double>();
                    y = obj["posY"].as<double>();
                    z = obj["posZ"].as<double>();
                    w = obj["width"].as<double>();
                    h = obj["height"].as<double>();
                } catch (...) {
                    printf(PRT_WARN"\tFailed to load Entity[Square] \n");
                    continue;
                }
                add_entity(new Square(x,y,z,w,h));
                entity_last()->name = name.substr(0,NAME_LEN);
                ntype=1;
                if(verbose) printf(PRT_MSG"\tLoaded Entity[Square] '%s' into scene at: (%g,%g,%g) \n",name.c_str(),x,y,z);

            } else if (type == "cuboid") {
                try {
                    name = obj["name"].as<std::string>();
                    x = obj["posX"].as<double>();
                    y = obj["posY"].as<double>();
                    z = obj["posZ"].as<double>();
                    w = obj["width"].as<double>();
                    h = obj["height"].as<double>();
                    d = obj["depth"].as<double>();
                } catch (...) {
                    printf(PRT_WARN"\tFailed to load Entity[Cuboid] \n");
                    continue;
                }

                add_entity(new Cuboid(x,y,z,w,h,d));
                entity_last()->name = name.substr(0,NAME_LEN);
                ntype=1;
                if(verbose) printf(PRT_MSG"\tLoaded Entity[Cuboid] '%s' into scene at: (%g,%g,%g) \n",name.c_str(),x,y,z);

            } else if (type == "sphere") {
                try {
                    name = obj["name"].as<std::string>();
                    x = obj["posX"].as<double>();
                    y = obj["posY"].as<double>();
                    z = obj["posZ"].as<double>();
                    r = obj["radius"].as<double>();
                } catch (...) {
                    printf(PRT_WARN"\tFailed to load Entity[Sphere] \n");
                    continue;
                }

                add_entity(new Sphere(x,y,z,r));
                entity_last()->name = name.substr(0,NAME_LEN);
                ntype=1;
                if(verbose) printf(PRT_MSG"\tLoaded Entity[Sphere] '%s' into scene at: (%g,%g,%g) \n",name.c_str(),x,y,z);

            } else if (type == "imported") {
                try {
                    name = obj["name"].as<std::string>();
                    file = obj["file"].as<std::string>();
                    x = obj["posX"].as<double>();
                    y = obj["posY"].as<double>();
                    z = obj["posZ"].as<double>();
                } catch (...) {
                    printf(PRT_WARN"\tFailed to load Entity[Imported] \n");
                    continue;
                }

                if (obj["scale"]) {
                    scale = obj["scale"].as<double>();
                } else {
                    scale = 1.0;
                }

                add_entity(new Imported(file,scale));
                trans[0] = x; trans[1] = y; trans[2] = z;
                entity_last()->translate(trans);
                entity_last()->name = name.substr(0,NAME_LEN);
                ntype=1;
                if(verbose) printf(PRT_MSG"\tLoaded Entity[Imported] '%s' into scene at: (%g,%g,%g) \n",name.c_str(),x,y,z);

            } else {
                printf(PRT_WARN"\tUnhandled object of type '%s' \n",type.c_str());
                continue;
            }


            // Handle properties that are common to all entities (but not shared)
            if (ntype==1) {

                if (obj["dcol"]) {
                    col = obj["dcol"].as<int>();
                    entity_last()->set_color('d',col);
                }

                if (obj["rcol"]) {
                    col = obj["rcol"].as<int>();
                    entity_last()->set_color('r',col);
                }

                if (obj["tcol"]) {
                    col = obj["tcol"].as<int>();
                    entity_last()->set_color('t',col); 
                }

                if (obj["gcol"]) {
                    col = obj["gcol"].as<int>();
                    entity_last()->set_color('g',col); 
                }

                if (obj["acol"]) {
                    col = obj["acol"].as<int>();
                    entity_last()->set_color('a',col); 
                }

                if (obj["amult"]) {
                    amult = std::max(obj["amult"].as<double>(),0.0);
                    entity_last()->amult = fabs(amult);
                }

                if (obj["rough"]) {
                    rgh = obj["rough"].as<double>()/100.0;
                    entity_last()->set_rough(rgh,-1);
                    if (rgh > 0) any_rough = true;
                }

                if (obj["n"]) {
                    rfractidx = obj["n"].as<double>();
                    entity_last()->rfractn = fabs(rfractidx);
                }

                if (obj["rotX"]) {
                    rot = obj["rotX"].as<double>();
                    entity_last()->rotate('x',rot,nullptr);
                }

                if (obj["rotY"]) {
                    rot = obj["rotY"].as<double>();
                    entity_last()->rotate('y',rot,nullptr);
                }

                if (obj["rotZ"]) {
                    rot = obj["rotZ"].as<double>();
                    entity_last()->rotate('z',rot,nullptr);
                }

            }
        }

        // If there's no rough materials, we don't need to have lots of reflection samples
        if (!any_rough && (mcrr > 1)) {
            if(verbose) printf(PRT_WARN"\tSince zero entities have both roughness and specular reflectivity, only 1 reflection sample is required \n");
            mcrr = 1;
        }

        // Summarise to stdout
        if(verbose) {
            printf(PRT_MSG"\t" PRT_SPACE "\n");
            printf(PRT_MSG"\tNumber of entities: %d \n",num_entities());
            printf(PRT_MSG"\tNumber of lights:   %d \n",num_lights());
            if (any_rough && (mcrr == 1))
                printf(PRT_WARN"\tWith only 1 sample per reflection, rough objects will appear smooth \n");
            if (any_dist && (mcsr == 1))
                printf(PRT_WARN"\tWith only 1 sample per shadow ray, distributed light sources will cast sharp shadows \n");
            printf(PRT_MSG"\n");
        }
    }
}

Scene::~Scene(){
    // Delete entities
    for(int i = 0; i < num_entities(); i++) {
        delete entities.at(i);
    }
    entities.shrink_to_fit();
    
    // Delete lights
    for(int i = 0; i < num_lights(); i++) {
        delete lights.at(i);
    }
    lights.shrink_to_fit();
}

// Registers an entity pointer with the scene
void Scene::add_entity(Entity* e){
    entities.push_back(e);
    selected_entities.clear();
    selected_entities.push_back(e);
}

// Registers a light source pointer with the scene
void Scene::add_light(Light* l){
    lights.push_back(l);
    selected_lights.clear();
    selected_lights.push_back(l);
}

// Returns the number of entities currently loaded into the scene
int Scene::num_entities(){
    return (int) entities.size();
}

// Returns the number of lights currently loaded into the scene
int Scene::num_lights(){
    return (int) lights.size();
}

// Returns a pointer to the entity of index 'idx'
Entity* Scene::entity_byIdx(int idx){
    if (idx < 0 || idx >= num_entities()) {
        printf(PRT_ERROR"In entity_byIdx(): Entity index %d out of range \n",idx);
        exit(EXIT_FAILURE);
    }
    return entities.at(idx);
}

// Returns a pointer to the light of index 'idx'
Light* Scene::light_byIdx(int idx){
    if (idx < 0 || idx >= num_lights()) {
        printf(PRT_ERROR"In light_byIdx():Light index %d out of range \n",idx);
        exit(EXIT_FAILURE);
    }
    return lights.at(idx);
}

Entity* Scene::entity_last() {
    return entity_byIdx(num_entities()-1);
}

Light* Scene::light_last() {
    return light_byIdx(num_lights()-1);
}

double Scene::sample_gaussian(){
    return dist(gen);
}

void Scene::clear_selected(){
    selected_entities.clear();
    selected_entities.shrink_to_fit();
    selected_lights.clear();
    selected_lights.shrink_to_fit();
}

void Scene::delete_selected(){
    for (int i = 0; i < (int)selected_entities.size(); i++) delete selected_entities[i];
    for (int i = 0; i < (int)selected_lights.size(); i++)   delete selected_lights[i];
    clear_selected();
}

void Scene::select_ents_by_type(char type) {
    selected_entities.clear();
    for (int i = 0; i < num_entities(); i++) {
        if (entities[i]->get_type() == type ) {
            selected_entities.push_back(entities[i]);
        }
    }
}

void Scene::select_lhts_by_type(char type) {
    selected_lights.clear();
    for (int i = 0; i < num_lights(); i++) {
        if (lights[i]->get_type() == type ) {
            selected_lights.push_back(lights[i]);
        }
    }
}

void Scene::select_ents_by_name(std::string ename) {
    selected_entities.clear();
    for (int i = 0; i < num_entities(); i++) {
        if (entities[i]->name == ename ) {
            selected_entities.push_back(entities[i]);
        }
    }
}

void Scene::select_lhts_by_name(std::string lname) {
    selected_lights.clear();
    for (int i = 0; i < num_lights(); i++) {
        if (lights[i]->name == lname ) {
            selected_lights.push_back(lights[i]);
        }
    }
}

void Scene::translate_selected(double x, double y, double z) {
    double v[3] = {x,y,z};
    for (int i = 0; i < (int)selected_entities.size(); i++) {
        selected_entities[i]->translate(v);
    }
    for (int i = 0; i < (int)selected_lights.size(); i++) {
        selected_lights[i]->translate(v);
    }
}

void Scene::rotate_selected(char ax, double degs) {
    for (int i = 0; i < (int)selected_entities.size(); i++) {
        selected_entities[i]->rotate(ax,degs,nullptr);
    }
    for (int i = 0; i < (int)selected_lights.size(); i++) {
        selected_lights[i]->rotate(ax,degs,nullptr);
    }
}

void Scene::setcol_selected(char coeff, int value) {
    if (coeff != 'e') {
        for (int i = 0; i < (int)selected_entities.size(); i++) {
            selected_entities[i]->set_color(coeff,value);
        }
    } else {
        for (int i = 0; i < (int)selected_lights.size(); i++) {
            selected_lights[i]->set_color(value);
        }
    }
}
