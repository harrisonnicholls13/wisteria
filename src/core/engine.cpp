#include "../wisteria.hpp"

#define NO_OBJ  (-4)
#define NO_FACE (-4)

/**
 * Find if a ray intersects an entity in the world, and where.
 * @param ray_src   Point from which ray originates (3D)
 * @param ray_dir   Direction of ray (3D)
 * @param scene     Object containing world information
 * @param obj_ex    Index of an entity to exclude, if any (NO_OBJ  otherwise)
 * @param face_ex   Index of a face to exclude, if any    (NO_FACE otherwise)
 * @param el        Extinction length
 * @param pass_tran Ignore occlusion by transparent objects (true/false)?
 * @param idor      Result: index of first occluding entity
 * @param cpor      Result: point where ray intersects occluding entity
 * @param fcor      Result: face of occluding entity that first contacts ray.
 * @returns         Does ray intersect any entity (true/false)?
 */
bool find_ray_occlusion_point(double* ray_src, double* ray_dir, Scene* scene, int obj_ex, int face_ex, double el,  bool pass_tran, int* idor, int* fcor, double* cpor){

    vunit(ray_dir,ray_dir);

    // Occlusion point
    bool   any_oc = false; // Any entity occludes?    
    double or_min_dist = INFINITY; // Min distance for o-ray.
    double temp_cp[3]; // temp storage for cp
    double test_cp[3]; // Tested entity cp
    int    temp_fc;    // temp storage for fc
    int    test_fc;    // Tested entity face index
    double test_dr[3]; // Test entity: vector from ray to cp
    double test_DD;    // norm(test_dr)
    bool   test_oc;    // Tested entity: does it occlude ray?
    double tcoeff[3];  // Tested entity transmission coeffs
    // Check direction
    double testdird;   // Dot product to test directions
    double testdirc[3];// Cross product ^^
    double tdc_norm;   // norm of ^^
    // double rsrc_p[3];  // ray_src + ray_dir * f
    // double rsrc_n[3];  // ray_src - ray_dir * f
    // double rdir_f[3];  // ray_dir * f
    // double rsrc_pd[3]; // Delta for rsrc_p
    // double rsrc_pdn;   // norm of ^^
    // double rsrc_nd[3]; // Delta for rsrc_m
    // double rsrc_ndn;   // norm of ^^
    // Work out what we need to record
    bool   specs;

    // Check if we want specifics
    if ( (idor == nullptr) || (cpor == nullptr) || (fcor == nullptr)) {
        // Make sure all of them are nullptr
        if ( (idor != nullptr) || (cpor != nullptr) || (fcor != nullptr) ) {
            printf(PRT_ERROR"fcor, idor, and cpor must either all be set or all be null, but not a combination of both \n");
            exit(EXIT_FAILURE);
        }
        specs = false;
    } else {
        specs = true;
    }

    // vscamul(rdir_f,ray_dir,+1e-10); vadd(rsrc_p,ray_src,rdir_f);
    // vscamul(rdir_f,ray_dir,-1e-10); vadd(rsrc_n,ray_src,rdir_f);
    

    // For each entity
    for (int i = 0; i < scene->num_entities(); i++) {
        if (i == obj_ex) continue; // Exclude excluded entity

        test_oc = scene->entity_byIdx(i)->find_contact(ray_src, ray_dir, &temp_fc, temp_cp);

        if (test_oc) {
            // Check if vec(src -> cp) is in the same direction ray
            vsub(test_dr,temp_cp,ray_src);
            vdot(&testdird,test_dr,ray_dir);
            vcross(testdirc,test_dr,ray_dir);
            vnorm(&tdc_norm,testdirc);
            if ( ( fabs(tdc_norm) > 1.0e-12) || (testdird < 0.0)) continue;

            // Check it's in the right direction (different/additional method)
            // vsub(rsrc_pd,temp_cp,rsrc_p); vnorm(&rsrc_pdn,rsrc_pd);
            // vsub(rsrc_nd,temp_cp,rsrc_n); vnorm(&rsrc_ndn,rsrc_nd);
            // if (rsrc_ndn < rsrc_pdn) continue;


            // Check if the occluding entity is entirely transparent
            if (pass_tran) {
                vequal(tcoeff,scene->entity_byIdx(i)->get_coeff('t',temp_fc,temp_cp));
                // if ( (tcoeff[0]==1) && (tcoeff[1]==1) && (tcoeff[2]==1) ) continue;
                if ( (tcoeff[0]>0) || (tcoeff[1]>0) || (tcoeff[2]>0) ) continue;
            }

            test_fc = temp_fc;
            vequal(test_cp, temp_cp);
            vsub(test_dr,test_cp,ray_src);
            vnorm(&test_DD,test_dr);

            if ((test_DD < el) && (test_DD < or_min_dist)) {

                any_oc = true;
                if (!specs) {
                    // If just testing for occlusion at all
                    return any_oc;
                }

                // Closest entity so far
                or_min_dist = test_DD;
                *idor = i;
                *fcor = test_fc;
                vequal(cpor,test_cp);
            }
        }
    }
    // if (any_oc && specs) printf(PRT_MSG"idor = %d, fcor = %d \n",*idor, *fcor);
    return any_oc;
}


/**
 * Helper function for monte-carlo ray tracing model.
 * This function is called recursively, as it handles an individual bounce+scatter event
 * @param in_src    Incident ray source location
 * @param in_dir    Incident ray direction
 * @param scene     object containing current scene
 * @param cur_med   Index of entity (medium) through which the incident ray is travelling (-1 for air)
 * @param ttl       Current time-to-live
 * @param out       Result: contribution of this ray (and deeper)
 * @returns         Did ray hit something? (true/false)
 */
bool _step_montecarlo(double* in_src, double* in_dir, Scene* scene, int cur_med, int ttl, double* out) {

    // Normalise incident ray's direction vector
    vunit(in_dir,in_dir);

    // Set up initial values for occlusion check
    int idx      = NO_OBJ;      // Index of object that incident ray hits
    double cp[3] = {0.0};       // Contact point of hit
    int fc       = NO_FACE;     // Index of face that incident ray hits

    // Does the incident ray hit anything?
    bool inc_occluded = find_ray_occlusion_point(in_src,in_dir,scene,NO_OBJ,NO_FACE,scene->mcel,false,&idx,&fc,cp);
    if (!inc_occluded) {
        return false;
    } 

    // Check that things look okay
    if ( (idx == NO_OBJ) || (fc == NO_FACE) ) {
        printf(PRT_ERROR"Something has gone wrong in find_ray_occlusion_point(). Exiting. \n");
        exit(EXIT_FAILURE);
    }

    // Entity vectors
    Entity* ent_medium;
    if (cur_med == -1) {
        ent_medium = nullptr;
    } else {
        ent_medium = scene->entity_byIdx(cur_med);
    }
    Entity* ent_contact = scene->entity_byIdx(idx);

    // Find normal of occluding object
    double N[3]; vequal(N,ent_contact->get_normal(fc,cp)); 

    // Get direct reflectance coefficient of this object
    double dcoeff[3]; vequal(dcoeff,ent_contact->get_coeff('d',fc,cp));

    // Add global illumination contribution to total
    vadd(out,out,ent_contact->get_coeff('g',fc,cp));


    if ( (dcoeff[0]>0) || (dcoeff[1]>0) || (dcoeff[2]>0) ) {

        // Add contribution at hit point to the total
        // This can be done using the lambertian model
        // Loop over light sources and get their contribution
        Light* lo;    
        double lo_illum[3];       // contribution of current source in loop
        double el_eps = 0.01;     // epsilon on exlen
        double lam;               // lambertian
        bool   occluded = false;  // occluded by anything at all?
        double lopos[3];          // Position of light object being used (accounting for its distributed nature)
        double lcp[3];            // vector from cp to light source
        double lcp_norm;          // norm of lcp
        double lam_out[3] = {0.0};// lambertian illumination for a single source
        double lam_tot[3] = {0.0};// Total lambertian illumnation for all sources
        int    sr_samps;          // Number of shadow ray samples (usually just scene->mcsr)

        // For each light source
        for (int i = 0; i < scene->num_lights(); i++){
            lo = scene->light_byIdx(i);

            if (lo->point) {
                sr_samps = 1;
            } else {
                sr_samps = scene->mcsr;
            }

            // For each sampled shadow ray
            // https://en.wikipedia.org/wiki/Distributed_ray_tracing
            for (int j = 0; j < sr_samps; j++) {
                // Calculate shadow ray
                lo->sample(lopos,j);
                vsub(lcp,lopos,cp);
                vnorm(&lcp_norm,lcp);

                // Check extinction length
                if (lcp_norm > lo->ex_len) continue;

                // Get point where light source first hits. Draw ray from cp to light source, and see if it intersects something
                occluded = find_ray_occlusion_point(cp,lcp,scene,NO_OBJ,NO_FACE,lo->ex_len+el_eps,true,nullptr,nullptr,nullptr);
                
                // If the object can see the source, get intensity from source at this point
                if (!occluded){
                    // get illumination
                    lo->get_illum(lo_illum,cp); 

                    // add lambertian contribution
                    vdot(&lam,N,lcp);
                    lam = std::max(0.0,lam);
                    vscamul(lo_illum,lo_illum,lam);

                    // add reflectance contribution
                    velemul(lo_illum,lo_illum,dcoeff);

                    // add this light source's total contribution to sum
                    vadd(lam_out,lam_out,lo_illum);
                }
            }
            // Normalise lighting by number of shadow ray samples
            vscamul(lam_out,lam_out,1.0/sr_samps);
            vadd(lam_tot,lam_tot,lam_out);

        }

        // Add lambertian contribution to the total
        vadd(out,out,lam_out);

    }

    // Get entity's characteristics
    double rcoeff[3]; vequal(rcoeff,ent_contact->get_coeff('r',fc,cp));
    double tcoeff[3]; vequal(tcoeff,ent_contact->get_coeff('t',fc,cp));
    bool is_refl = ((rcoeff[0]>0) || (rcoeff[1]>0) || (rcoeff[2]>0));
    bool is_tran = ((tcoeff[0]>0) || (tcoeff[1]>0) || (tcoeff[2]>0));
    double rough = ent_contact->get_rough(fc,cp);

    // If no more steps remain, stop recursion here
    if ( ttl <= 0 ) {
        return true;
    }

    // If reflective or transmissive continue recursion ...
    if ( is_refl || is_tran ) {

        // Reduce number of steps left
        ttl--;

        // First, calculate 'central' reflection ray corresponding to the incident ray at equal angles
        // <http://paulbourke.net/geometry/reflected/>
        // R_r = R_i - 2 N dot(R_i,N) 
        double R_IdotN;     vdot(&R_IdotN, in_dir,N);
        double R_diff[3];  vequal(R_diff,N); vscamul(R_diff, R_diff, -2.0*R_IdotN); 
        double R_r0[3];  vadd(R_r0, in_dir, R_diff); vunit(R_r0,R_r0);
        double R_r02[3]; velemul(R_r02, R_r0, R_r0); // square each element of central reflection ray

        // Use central reflection ray to generate scattering rays
        // For each of these, there's an equivalent transmitted ray
        int    dim[3] =         {0};    // Which dimensions are calculated/randomised
        double pray_coeff[3] =  {0.0};  // pertubation ray calculation coefficients
        double pray[3] =        {0.0};  // pertubation ray itself

        double R_rn[3] =        {0.0};  // nth scattered reflection ray
        double refl_out[3] =    {0.0};  // Output of each reflected scattering exploration
        double refl_tot[3] =    {0.0};  // Total of all reflected scatter contributions
        bool   refl_hit =       false;  // Did a scattering ray hit something?
        int    refl_nhits =     0;      // Number of rays that hit something

        double mu = 1;  // mu
        double T1[3];   // n.sqrt(...)
        double T2[3];   // mu.in_dir
        double T3[3];   // -1.n(n.i)
        double T_rn[3] =        {0.0};  // nth scattered transmission ray
        double tran_out[3] =    {0.0};  // Output of transmission exploration
        double tran_tot[3] =    {0.0};  // Total of all of ^^
        bool   tran_hit =       false;  // Did the transmission ray hit something?
        int    tran_nhits =     0;      // Number of transmission rays that hit something


        // See: <https://en.wikipedia.org/wiki/Schlick%27s_approximation>
        double schl_R0 =        0.0;    // Offset to reflection factor (from Schlick's approx.)
        double schl_Rt =        0.0;    // Total reflection factor     (for a given ray, angle)
        double schl_Tt =        0.0;    // Total transmission factor   (^^)
        double schl_ct =        0.0;    // Cos(theta) term used in above formulae
        double schl_n1 =        1.0;    // Refractive index n1
        double schl_n2 =        1.0;    // Refractive index n2

        int nxt_med = -2; // medium for tran ray to enter

        if (cur_med == -1) {
            // air->entity
            schl_n1 = scene->nair;
            schl_n2 = ent_contact->rfractn;
            nxt_med = idx;

        } else if (cur_med == idx) {
            // entity->air
            schl_n1 = ent_contact->rfractn;
            schl_n2 = scene->nair;
            nxt_med = -1;

        } else if ( (cur_med != idx) && (idx > -1) && (cur_med > -1)){
            // entity->entity
            schl_n1 = ent_medium->rfractn;
            schl_n2 = ent_contact->rfractn;
            nxt_med = idx;
        }

        // If hit entity doesn't have volume, don't transmit though it 
        if (!ent_contact->is_volumetric()) nxt_med = cur_med;

        mu = (schl_n1/schl_n2);
        schl_R0 = (schl_n1 - schl_n2)/(schl_n1 + schl_n2);
        schl_R0 = schl_R0 * schl_R0;

        // For each reflection ray
        for (int i = 0; i < scene->mcrr; i++) { 

            vset(R_rn,0.0);

            if (i == 0) {
                // First ray must be equal to central reflection ray
                vequal(R_rn,R_r0);
            } else {
                // calculate which dimension will be calculated, with others randomised
                dim[0] = (dim[0]+1) % 3;
                dim[1] = (dim[0]+1) % 3;
                dim[2] = (dim[0]+2) % 3;

                // set two elements of pray_coeff to random numbers
                pray_coeff[dim[0]] = scene->sample_gaussian();
                pray_coeff[dim[1]] = scene->sample_gaussian();

                // calculate third element such that pray is perpendicular to R_r0
                pray_coeff[dim[2]] = -1.0/R_r02[dim[2]] * (pray_coeff[dim[0]] * R_r02[dim[0]] + pray_coeff[dim[1]] * R_r02[dim[1]]);

                // use these coeffs to calculate the pertubation ray
                velemul(pray,pray_coeff,R_r0);
                vunit(pray, pray);
                vscamul(pray,pray,rough); // scale the pray by surface roughness

                // use pertubation ray to calculate scattered reflection ray
                vadd(R_rn, R_r0, pray);
            }
            vunit(R_rn, R_rn);

            // Use Schlick's approximation to find reflection/transmission factors
            // Technically, in this term should be the incident vector, but we use R_rn
            // to account for the scattering processes
            vdot(&schl_ct,N,R_rn);
            schl_Rt = schl_R0 + (1-schl_R0) * pow(1 - schl_ct,5.0);
            schl_Tt = 1 - schl_Rt;

            if (is_refl){
                // generate new reflection recursion step,
                // new medium is source medium, as reflection ray doesn't enter entity idx
                refl_hit = _step_montecarlo(cp,R_rn,scene,cur_med,ttl,refl_out);
                if (refl_hit) { 
                    // Add this reflection step's contribution to total of all reflection scattering rays
                    vscamul(refl_out,refl_out,schl_Rt);
                    vadd(refl_tot, refl_tot, refl_out);
                    refl_nhits++;
                }
                
            }
            
            if (is_tran && (i==0)) {
                // generate new transmission recursion step, but only for the central reflection ray
                // use snells law to find refraction ray corresponding to the pseudo-incident ray
                // https://physics.stackexchange.com/a/436252

                vset(T_rn,0.0);

                // Calculate T1
                vscamul(T1,N,sqrt(1.0 - mu*mu * (1.0 - R_IdotN*R_IdotN)));
                vadd(T_rn,T_rn,T1);

                // Calculate T2
                vscamul(T2,in_dir,mu);
                vadd(T_rn,T_rn,T2);

                // Calculate T3
                vscamul(T3,N,1.0 * mu * R_IdotN);
                vadd(T_rn,T_rn,T3);

                tran_hit = _step_montecarlo(cp,T_rn,scene,nxt_med,ttl,tran_out);
                if (tran_hit) {
                    // Add this transmission step's contribution to total of all transmission scattering rays
                    vscamul(tran_out,tran_out,schl_Tt);
                    vadd(tran_tot,tran_tot,tran_out);
                    tran_nhits++;
                }
            }

        }

        // Add reflected scatter ray contribution to total
        if (is_refl && (refl_nhits > 0)) {
            vscamul(refl_tot,refl_tot,1.0/scene->mcrr);
            velemul(refl_tot,refl_tot,rcoeff);
            vadd(out,out,refl_tot);
        }

        // Add transmission scatter ray contribution to total
        if (is_tran && (tran_nhits > 0)) {
            velemul(tran_tot,tran_tot,tcoeff);
            vadd(out,out,tran_tot);
        }
    }

    // Attenuation stuff for volumetric rendering
    // https://en.wikipedia.org/wiki/Radiative_transfer
    double acoeff[3] = {0.0};
    double amult = 1.0;
    if (cur_med == -1) {
        vset(acoeff,1.0); // air absorbs all colors equally
        amult = scene->amultair;
    } else {
        vequal(acoeff,ent_medium->get_coeff('a',fc,cp));
        amult = ent_medium->amult;
    }
    double etau[3] = { 0.0 };
    double path_len = 0.0;
    double path_vec[3] = {0.0};
    double tau[3] = {0.0};
    
    // Work out path between source and cp
    vsub(path_vec,cp,in_src);
    vnorm(&path_len,path_vec);

    // Calculate optical depth of material
    // Assumes absorption coefficient is constant throughout entity and there's no emission
    vequal(tau,acoeff);
    vscamul(tau,tau,path_len*amult);
    
    // calculate exp(-tau)
    for (int itau = 0; itau < 3; itau++) {
        if (tau[itau] >= scene->taumax) {
            etau[itau] = 0.0;
        } else {
            etau[itau] = exp(-1.0 * tau[itau]);
        }
    }

    // Apply attenuation to total ray value
    velemul(out,out,etau);


    //Check that 'out' is exclusively positive
    // for (int i = 0; i < 3; i++) {
    //     if (out[i] < 0) {
    //         printf("\n");
    //         printf(PRT_ERROR"In _step_montecarlo(): Radiation ray has negative colour component! out=(%g,%g,%g) \n",out[0],out[1],out[2]);
    //         exit(EXIT_FAILURE);
    //     }
    // }

    return true;
}


/**
 * Determine brighness at a point due to all light sources in the scene, with monte-carlo model
 * Includes illumination and reflection, as well as configurable surface reflectance
 * @param upr       primary ray corresponding to this pixel
 * @param scene     object containing current scene
 * @param out       Result: illumination at that point
 */
void ray_explore_montecarlo(double* upr, Scene* scene, double* out) {
    // This function acts as a wrapper around the function that recursively calculates the ray value

    // Set initial result value
    vset(out,0.0);
    // Make initial call to recursion function with 1 ray from camera to cp
    _step_montecarlo(scene->cam_pos,upr,scene,-1,scene->rttl,out);
}


/**
 * Determine value of a single pixel due to entities and lights in the scene
 * @param scene     Object containing current scene
 * @param upr       Primary ray for this pixel
 * @param ray_res   Result: 1D vector value of this pixel found by the ray, format:[r,g,b,a]
 */
void do_ray(Scene* scene, double* upr, double* ray_res){
    // Set up initial values
    int idx      = -1;
    double cp[3] = {0.0};
    int fc       = -1;

    // Does ray hit anything?
    bool occluded = find_ray_occlusion_point(scene->cam_pos,upr,scene, NO_OBJ, NO_FACE, scene->prel,false,&idx,&fc,cp);
    double illum[3] = {0.0};
    // If yes, work out illumination at that point
    if (occluded) {
        // Call ray-tracing code
        ray_explore_montecarlo(upr, scene, illum);
        // camera-to-cp effects
        double dist2 = 0.0;
        double vec[3];
        vsub(vec,cp,scene->cam_pos); vdot(&dist2,vec,vec); dist2 = 1.0/dist2;
        vscamul(illum,illum,dist2);
        // Save result
        ray_res[0] = illum[0];
        ray_res[1] = illum[1];
        ray_res[2] = illum[2];
        ray_res[3] = 1.0;
    }
    
}


/**
 * Render a sub-array of pixels in the camera. Non-rendered pixels are set to 0
 * @param scene     Object containing current scene
 * @param start     Flattened pixel index to start render at
 * @param end       Flattened pixel index to stop render at
 * @param upr_mat   Matrix of unit primary rays
 * @param out       Result: chunk's render output matrix, of shape (cam_cs,4)
 */
void do_chunk(Scene* scene, int start, int end, double** upr_mat, double** out, double* pct) {
    int count = 0;  // percentage tracker
    for (int i = 0; i < scene->cam_nix; i++) {
        if ( (i >= start) && (i < end) ){
            // Call do_ray() to render a result for a single pixel
            // printf(PRT_MSG"do ray: %d\n",i);
            do_ray(scene,upr_mat[i],out[count]);
            count++;
        }
        *pct = (double) count / (end-start) * 10.0;
    }
}


/**
 * Run ray tracing engine, and save result to file
 * @param scene     Object containing current scene
 */
void run_engine(Scene* scene){
    printf(PRT_MSG"Running engine... \n");

    // Work out how verbose we need to be
    bool verbose = (scene->verbose || (DEBUG>0));

    // Calcuating pixel positions and primary rays
    if(verbose) printf(PRT_MSG"\tCalculating camera position and UPRs \n");
    double** pix = alloc2D(scene->cam_nix,3); // Matrix of pixel positions
    double** upr = alloc2D(scene->cam_nix,3); // Matrix of unit primary-rays
    int      pin = 0;                         // Pixel index
    double pix_pos[3];
    double** mrot = alloc2D(3,3);
    for (int px = 0; px < scene->cam_rx; px++) {
        for (int py = 0; py < scene->cam_ry; py++){
            // Set-up at origin
            vset(pix_pos,0.0);
            pix_pos[0] = ((float)px/scene->cam_rx - 0.5) * scene->cam_dx;
            pix_pos[1] = scene->cam_dz;
            pix_pos[2] = ((float)py/scene->cam_ry - 0.5) * scene->cam_dy;
            // Rotate to point at target
            rotmat3D(mrot,'y',180.0);           vmatmul(pix_pos,mrot,pix_pos);
            rotmat3D(mrot,'z',scene->cam_long); vmatmul(pix_pos,mrot,pix_pos);
            rotmat3D(mrot,'x',scene->cam_lati); vmatmul(pix_pos,mrot,pix_pos);
            
            // Translate to camera position
            vadd(pix_pos,pix_pos,scene->cam_pos);
            // Save to matrices
            vequal(pix[pin],pix_pos); // set pix
            vsub(pix_pos,pix_pos,scene->cam_pos); vunit(pix_pos,pix_pos); vequal(upr[pin],pix_pos); // set upr
            pin++;
       }
    }
    free2D(mrot,3);

    /**
     * It is necessary to pre-chunk the pixel grid, but not in the same way as in the Python version.
     * Instead, just tell each process which flattened pixel index to start at (idx = py + px*width)
     * Each should do `chunksize` num of pixels, except the last one.
     */

    // Work out where chunks start
    if(verbose) printf(PRT_MSG"\tChunking grid \n");
    int* chunk_bounds = (int*) new int[scene->tmax+1];
    int cbi = 0;
    for (int i = 0; i < scene->cam_nix; i++) {
        if ( i % scene->cam_cs == 0){
            // End of chunk
            chunk_bounds[cbi] = i;
            cbi++;
        }
    }
    chunk_bounds[scene->tmax] = scene->cam_nix;

    // Main ray-tracing code
    if(verbose) printf(PRT_MSG"\tRay tracing... \n");

    // Output arrays
    double**    rres = alloc2D(scene->cam_nix,4);             // final result: r, g, b, a (alpha = 0.0 if unvisited)
    double***   cres = alloc3D(scene->tmax,scene->cam_cs,4);  // for each chunk to store its results in, in flattened form
    double*     pcts = alloc1D(scene->tmax);                  // array to track how complete each chunk is (percentage)

    // Dispatch threads:
    std::thread* procs = new std::thread[scene->tmax];
    for (int i = 0; i < scene->tmax; i++){
        procs[i] = std::thread(do_chunk, scene, chunk_bounds[i], chunk_bounds[i+1], upr, cres[i], &pcts[i]);
    }

    // Print progress while rendering
    double pct =      0.0;   // current stored percentage
    double newpct =   0.0;   // new pct to check
    int    prt_pct =  0.0;   // int used to print out
    double avg =    0.0; // average
    int proglen =   0;   // length of progress bar

    // Calculate length of progress bar; can't be shorter than 20 chars
    proglen = scene->tmax*4 - 1;
    if (proglen < 20) proglen = 20; 

    // Print progress while code is running
    printf(PRT_MSG"\t(Starting)\n"); fflush(stdout);
    do {
        avg = 0.0;
        for (int i = 0; i < scene->tmax; i++) {
            avg += pcts[i]*10.0; 
        }
        newpct = roundTo(avg/scene->tmax,1);
        if  (newpct > pct){
            pct = newpct;
            prt_pct = (int) ceil(newpct);
            printf("\33[2K\r");
            printf("\033[A\r");
            printf("\33[2K\r");
            printf(PRT_MSG"\tProgress: ["); 
            for (int i = 0; i < proglen; i++) { // print progress bar
                if ( (i/(double)proglen*100.0) < pct) {
                    printf("#");
                } else {
                    printf(" ");
                }
            }
            printf("] %03d/100\n,",prt_pct); // print total
            printf(PRT_MSG"\tBy chunk: [");
            for (int i = 0; i < scene->tmax; i++) {  // print by chunk
                if (i > 0) printf("|");
                if (pcts[i] < 9.9) {
                    printf(" %1d ",(int)pcts[i]);
                } else {
                    printf(" X ");
                }
            }
            printf("] /10");
            fflush(stdout);
        }

        // Sleep for bit
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    } while (pct < 100);
    printf("\n");

    //Join threads:
    if(verbose) printf(PRT_MSG"\tJoining threads \n");
    for (int i = 0; i < scene->tmax; i++)
        procs[i].join();

    if(verbose) printf(PRT_MSG "\t" PRT_SPACE "\n");

    // Compile results
    if(verbose) printf(PRT_MSG"\tCombining chunks\n");
    int chunk = 0;      // Which chunk are we currently in?
    int cocum = 0;      // cumulative counter for compiling render
    for (int i = 0; i < scene->cam_nix; i++) {    // for each pixel
        if (i == chunk_bounds[chunk+1]){
            chunk++; // move to new chunk
            cocum = 0;
        }
        for (int j = 0; j < 4; j++) {  // for each color value (r,g,b,a)
            rres[i][j] += cres[chunk][cocum][j];
        }
        cocum++;
    }

    // Gamma correction
    if (scene->gamma != 1.0) {
        if(verbose) printf(PRT_MSG"\tApplying gamma \n");
        for (int i = 0; i < scene->cam_nix; i++) {  // for each pixel
            for (int j = 0; j < 3; j++) {          // for each color value (r,g,b,a)
                rres[i][j]  = pow(rres[i][j],scene->gamma);
            }
        }
    }


    // Normalising results
    if(verbose) printf(PRT_MSG"\tNormalising pixels \n");
    std::vector<double> rsorted;                // make a copy of rres and sort it
    for (int i = 0; i < scene->cam_nix; i++) {
        for (int j = 0; j < 3; j++) {
            rsorted.push_back(rres[i][j]);
        }
    }
    std::sort(rsorted.begin(),rsorted.end());
    double mpx = rsorted[(int) (scene->cam_nix*3*(scene->max_pct/100.0))-1];
    double nfac = 255/mpx; // factor to apply to each pixel to normalise data
    for (int i = 0; i < scene->cam_nix; i++) {
        for (int j = 0; j < 3; j++) {
            // Apply factor and limit result to range [0,255]
            rres[i][j] = rres[i][j] * nfac;
            if (rres[i][j] > 255) rres[i][j] = 255;
            if (rres[i][j] < 0)   rres[i][j] = 0;
        }
    }

    // Save to file
    std::string image_type = scene->outfile.substr(scene->outfile.size()-3,3);
    if (image_type == "ppm") {
        writePPM(scene,rres);
    } else if (image_type == "png") {
        writePNG(scene,rres);
    } else {
        printf(PRT_WARN"\tInvalid image container '%s'; using PPM \n",image_type.c_str());
        writePPM(scene,rres);
    }
    printf(PRT_MSG"Saved image to '%s' \n",scene->outfile.c_str());

    // Free memory
    free2D(upr,scene->cam_nix);
    free2D(pix,scene->cam_nix);
    delete [] chunk_bounds;
    free1D(pcts);
    free2D(rres,scene->cam_nix);
    free3D(cres,scene->tmax,scene->cam_cs);
}



