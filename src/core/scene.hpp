#include "../wisteria.hpp"

class Scene {

    // Variables
    private:
        // World objects
        std::vector<Entity*> entities;
        std::vector<Light*>  lights;
        // Random generator
        std::mt19937 gen;
        std::normal_distribution<> dist;
    public:
        // Camera properties
        float   cam_res =   200;            // Resoltion factor
        float   cam_dx =    4;              // Geometrical width
        float   cam_dy =    3;              // Geometrical height
        float   cam_dz =    12;             // Geometrical depth (corresponds with focal length)
        double  cam_pos[3]= {0,-16,30};     // Camera position
        double  cam_lati =  -60.0;          // Camera latitudinal tilt angle (degs)
        double  cam_long =  0.0;            // Camera longitudinal tilt angle (degs)

        // Render properties
        int     tmax =      1;          // Max threads
        double  prel =      500.0;      // Render distance
        int     rttl =      1;          // Ray initial time-to-live
        double  mcel =      300.0;      // Monte carlo ray extinction length
        int     mcrr =      1;          // Monte carlo reflection ray samples
        int     mcsr =      1;          // Number of shadow rays to cast
        double  nair =      1.0;        // Refractive index of air
        double  taumax =    20.0;       // Max optical depth
        double  amultair =  0.02;       // Attenuation multiplier of air

        // Post-processing options
        std::string outfile =   "res/out.ppm";      // Output file
        double      max_pct =   99;                 // Maximum percentile when clipping data 
        int         bgCol[3] =  {200,50,200};       // Scene background color
        float       gamma =     0.2;                // Gamma correction factor
        float       usf =       1.0;                // Upscale factor at write-time

        // Derived properties (default parameters are always overwritten)
        int cam_rx  = 0; // pixel resolution in x direction
        int cam_ry  = 0; // pixel resolution in y direction
        int cam_cs  = 0; // chunk size (pixels per chunk)
        int cam_nix = 0; // total number of pixels

        // Interface variables
        bool                 verbose = false;         // Verbosity for printing
        std::vector<Entity*> selected_entities;     // Entities which are currently selected for modification
        std::vector<Light*>  selected_lights;       // Lights which are currently selected for modification

    // Functions
    public:
                Scene(std::string filepath, bool verbose);
                ~Scene();
        void    add_entity(Entity* e);
        void    add_light(Light* l);
        double  sample_gaussian();
        Entity* entity_byIdx(int idx);
        Light*  light_byIdx(int idx);
        int     num_entities();
        int     num_lights();
        Entity* entity_last();
        Light*  light_last();
        void    clear_selected();
        void    delete_selected();
        void    select_ents_by_type(char type);
        void    select_lhts_by_type(char type);
        void    select_ents_by_name(std::string ename);
        void    select_lhts_by_name(std::string lname);
        void    translate_selected(double x, double y, double z);
        void    rotate_selected(char ax, double degs);
        void    setcol_selected(char coeff, int value);
};


