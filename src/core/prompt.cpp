#include "../wisteria.hpp"

#define STREQ(s,x)      (strcmp(s,x) == 0 ? 1 : 0)  
#define CMDLEN          (128)

/**
 * Parse a single command string for a prompt/script
 * @param scene     Scene pointer to work on
 * @param cmd       Command string
 * @returns         Exit code for command (0=commands, 1=run, 2=exit)
 */
int parse_cmd(Scene* scene, char cmd[CMDLEN]) {

    // Check if this line is a comment
    if (cmd[0] == '#') {
        return 0;
    } 

    const char  cmd_dlm[2]  = " ";      // Delimeter
    char**      cmd_arr     = NULL;     // Stores user commands
    char*       cmd_p       = NULL;     
    int         cmd_n       = 0;        // Length of user command
    int         result      = 0;        // What to do after this command has been run (0 is default)
    int         added_sort = 0;        // Sort of thing added by command (0=entity, 1=light)

    // Loop through input
    cmd_p = strtok(cmd,cmd_dlm);
    while(cmd_p) {
        cmd_arr = (char**) realloc(cmd_arr, sizeof(char*) * ++cmd_n);
        if (cmd_arr == NULL) {
            printf(PRT_ERROR"Failed to allocate memory for command \n");
            exit(EXIT_FAILURE);
        }
        cmd_arr[cmd_n - 1] = cmd_p;
        cmd_p = strtok(NULL,cmd_dlm);
    }
    cmd_arr = (char**) realloc(cmd_arr, sizeof(char*) * (cmd_n + 1));
    cmd_arr[cmd_n] = 0;

    // Handle command supplied by user
    if ( STREQ(cmd_arr[0],"load") && (cmd_n == 2) ){
        // Load in scene from yaml file, overwriting the old one
        delete scene;
        scene = new Scene(cmd_arr[1], true);
        scene->verbose = false;

    } else if ( STREQ(cmd_arr[0],"exec") && (cmd_n == 2) ){
        // Run script file (does not fork)
        result = run_script(scene,std::string(cmd_arr[1]));

    } else if ( STREQ(cmd_arr[0],"run") ){
        // Run the renderer
        result = 1;

    } else if ( STREQ(cmd_arr[0],"exit") ) {
        // Exit program 
        result = 2;

    } else if ( STREQ(cmd_arr[0],"add") && ( (cmd_n == 3) || (cmd_n == 4) ) && (scene != nullptr) ) {
        // Add entity/light of given type
        // e.g. "add cuboid garry"
        // e.g. "add imported monkey res/objs/suzanne.obj"
        added_sort = 0;

        if ( STREQ(cmd_arr[1],"sun") ) {
            scene->add_light( new Sun(0,0,0,1,1));
            added_sort = 1;
        } else if ( STREQ(cmd_arr[1],"cuboid") ) {
            scene->add_entity(new Cuboid(0,0,0,1,1,1));
        } else if ( STREQ(cmd_arr[1],"sphere") ) {
            scene->add_entity(new Sphere(0,0,0,1));
        } else if ( STREQ(cmd_arr[1],"imported") && (cmd_n == 4)) {
            scene->add_entity(new Imported(std::string(cmd_arr[3]),1.0));
        } else {
            printf(PRT_WARN"Unrecognised object type '%s' \n",cmd_arr[1]);
            return 0;
        }

        // Set name
        if (added_sort == 0) {
            scene->entity_last()->name = std::string(cmd_arr[2]).substr(0, NAME_LEN);
        } else {
            scene->light_last()->name = std::string(cmd_arr[2]).substr(0, NAME_LEN);
        }


    } else if ( STREQ(cmd_arr[0],"select") && (cmd_n == 3) && (scene != nullptr)) {

        // Different selection methods
        if ( STREQ(cmd_arr[1],"type") ) { // Select by type
            if ( STREQ(cmd_arr[2],"sun") ) {
                scene->select_lhts_by_type('s');
            } else if ( STREQ(cmd_arr[2],"cuboid") ) {
                scene->select_ents_by_type('c');
            } else if ( STREQ(cmd_arr[2],"sphere") ) {
                scene->select_ents_by_type('x');
            } else if ( STREQ(cmd_arr[2],"imported") ) {
                scene->select_ents_by_type('i');
            } else {
                printf(PRT_WARN"Unrecognised type '%s' \n",cmd_arr[2]);
            }
        } else if ( STREQ(cmd_arr[1],"name") ) { // Select by name
            scene->select_ents_by_name(std::string(cmd_arr[2]));
            scene->select_lhts_by_name(std::string(cmd_arr[2]));
        } else {
            printf(PRT_WARN"Unrecognised selection filter '%s' \n",cmd_arr[1]);
            return 0;
        }


    } else if ( STREQ(cmd_arr[0],"delete") && (scene != nullptr)) {
        scene->delete_selected();

    } else if ( STREQ(cmd_arr[0],"clear") && (scene != nullptr)) {
        scene->clear_selected();

    } else if ( STREQ(cmd_arr[0],"list") && (scene != nullptr)) {
        std::string stype = "";
        printf(PRT_MSG"\t│ Index │   Type   │       Name       │ \n");
        printf(PRT_MSG"\t├───────┼──────────┼──────────────────┤ \n");

        // List entities first
        for (int i = 0; i < scene->num_entities(); i++) {
            switch (scene->entity_byIdx(i)->get_type()){
                case 'x': stype = "Sphere  "; break;
                case 'c': stype = "Cuboid  "; break;
                case 's': stype = "Square  "; break;
                case 'i': stype = "Imported"; break;
            }
            printf(PRT_MSG"\t│ %-5d │ %-8s │ %-16s │\n",i,stype.c_str(),scene->entity_byIdx(i)->name.c_str());
        }

        // List light sources
        for (int i = 0; i < scene->num_lights(); i++) {
            switch (scene->light_byIdx(i)->get_type()){
                case 's': stype = "Sun     "; break;
            }
            printf(PRT_MSG"\t│ %-5d │ %-8s │ %-16s │\n",i,stype.c_str(),scene->light_byIdx(i)->name.c_str());
        }

    } else if ( STREQ(cmd_arr[0],"translate") && (cmd_n == 4) && (scene != nullptr) ) {
        double tx = 0; double ty = 0; double tz = 0; 
        try {
            tx = std::stod(std::string(cmd_arr[1]));
            ty = std::stod(std::string(cmd_arr[2]));
            tz = std::stod(std::string(cmd_arr[3]));
        } catch (...) {
            printf(PRT_WARN"Could not parse vector components \n");
            return 0;
        }
        scene->translate_selected(tx,ty,tz);

    } else if ( STREQ(cmd_arr[0],"rotate") && (cmd_n == 3) && (scene != nullptr) ) {
        char ax = 'u';
        if ( STREQ(cmd_arr[1],"x") ) {
            ax = 'x';
        } else if ( STREQ(cmd_arr[1],"y") ) {
            ax = 'y';
        } else if ( STREQ(cmd_arr[1],"z") ) {
            ax = 'z';
        } else {
            printf(PRT_WARN"Invalid axis \n");
            return 0;
        }
        double degs = std::stod(std::string(cmd_arr[2]));
        scene->rotate_selected(ax,degs);

    } else if ( STREQ(cmd_arr[0],"color") && (cmd_n == 3) && (scene != nullptr) ) {
        char coeff = 'u';
        if ( STREQ(cmd_arr[1],"diffuse") ) {
            coeff = 'd';
        } else if ( STREQ(cmd_arr[1],"glossy") ) {
            coeff = 'g';
        } else if ( STREQ(cmd_arr[1],"transmission") ) {
            coeff = 't';
        } else if ( STREQ(cmd_arr[1],"attenuation") ) {
            coeff = 'a';
        } else if ( STREQ(cmd_arr[1],"emission") ) {
            coeff = 'e';
        }else {
            printf(PRT_WARN"Unrecognised coefficient \n");
            return 0;
        }
        int value = (int)strtoul(cmd_arr[2], NULL, 0);
        scene->setcol_selected(coeff,value);

    } else if ( STREQ(cmd_arr[0],"help") ) {
        //print help menu
        printf(PRT_MSG"Available commands are as follows...\n");

        printf(PRT_MSG"load [F] \n");
        printf(PRT_MSG"\tLoad a static scene configuration file [F] written in a YAML format \n");
        printf(PRT_MSG"\tThis command overwrites the current scene configuration \n");

        printf(PRT_MSG"exec [F] \n");
        printf(PRT_MSG"\tExecutes a script file [F] \n");

        printf(PRT_MSG"run \n");
        printf(PRT_MSG"\tRuns the rendering engine with the current scene and exits \n");

        printf(PRT_MSG"add [O] [N] [F] \n");
        printf(PRT_MSG"\tAdd an object (which can be an entity or a light source) of type [O] to the scene\n");
        printf(PRT_MSG"\tIt will be given the name [N]. For 'Imported' entities, [F] is the .obj file path \n");

        printf(PRT_MSG"select [S] [V] \n");
        printf(PRT_MSG"\tSelect all objects which match the provided filter. Setting [S] allows filtering by 'name' or 'type'\n");
        printf(PRT_MSG"\tThe value of [V] determines the filter value to use \n");

        printf(PRT_MSG"list \n");
        printf(PRT_MSG"\tList all objects in the scene\n");

        printf(PRT_MSG"delete \n");
        printf(PRT_MSG"\tDeletes all selected objects\n");

        printf(PRT_MSG"clear \n");
        printf(PRT_MSG"\tClears the current object selection\n");

        printf(PRT_MSG"exit \n");
        printf(PRT_MSG"\tExits the program \n");

        printf(PRT_MSG"help \n");
        printf(PRT_MSG"\tPrints this help menu \n");

        printf(PRT_MSG"\n");

    } else {
        // catch
        printf(PRT_WARN"Invalid or improperly formatted command '%s'. Type 'help' for help \n",cmd);
    }


    return result;
}

/**
 * Run the input prompt for the user to command the program
 * @param scene     Scene pointer to work on
 * @returns         Resulting action once prompt is complete (1=run, 2=exit)
 */
int run_prompt(Scene* scene) {

    int         prompt_code = 0;        // Prompt command intent (0=commands, 1=run, 2=exit)
    char        input[CMDLEN]  = "";


    // Main prompt loop - take commands and parse them
    while(prompt_code == 0) {
        // Read prompt
        printf(PRT_PRMPT);
        scanf(" %127[^\n]",input);
        fflush(stdin);

        // Parse command and handle result
        prompt_code = parse_cmd(scene,input);

    }

    return prompt_code;

}

/**
 * Execute a script of commands
 * @param scene     Scene pointer to work on
 * @returns         Resulting action once script is complete (0=prompt, 1=run, 2=exit(good), 3=exit(bad))
 */
int run_script(Scene* scene, std::string script_path) {
    std::ifstream file(script_path);

    // https://stackoverflow.com/a/51572325

    std::string line;
    char cmd[CMDLEN];
    int cmd_result = 0;

    // Check if file is readable
    if (file.is_open()) {
        printf(PRT_MSG"Reading script file '%s' \n",script_path.c_str());
        while (std::getline(file, line)) {

            // Check if line is too long
            if (line.size() >= 128) {
                printf(PRT_WARN"Command has too many characters; ignoring \n");
                continue;
            }

            // Check if line is empty
            if (line.size() <= 0) continue;

            // Parse line with parse_cmd function
            strncpy(cmd, line.c_str(), sizeof(cmd));
            cmd_result = parse_cmd(scene,cmd);
            if (cmd_result != 0) break;
        }
        file.close();
    }

    printf(PRT_MSG"Script execution complete\n");
    return cmd_result;
}



