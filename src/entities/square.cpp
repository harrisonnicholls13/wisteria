#include "../wisteria.hpp"

/**
 * Constructor for square class
 * @param x         Position (x)
 * @param y         Position (y)
 * @param z         Position (z)
 * @param w         Width (x)
 * @param h         Height (y)
 */
Square::Square(double x, double y, double z, double w, double h) {

    // Create two triangles
    tris[0] = new Triangle(x,y,z,w,h);
    tris[1] = new Triangle(x+w,y+h,z,-w-olp,-h-olp);

    // Set position
    pos[0] = x+w/2; pos[1] = y+h/2; pos[2] = z;

}

/**
 * Descructor for square class
 */
Square::~Square(){
    delete tris[0];
    delete tris[1];
}

/**
 * Get entity position
 * @returns position vector (3D)
 */
double* Square::get_pos(){
    return pos;
}

/**
 * Is entity volumetric?
 * @returns true/false depending on entity type
 */
bool Square::is_volumetric(){
    return false;
}

/**
 * Return entity type as char
 * @returns Type char
 */
char Square::get_type(){
    return 's';
}


/**
 * Translate entity by a vector v
 * @param v         Vector to translate entity by
 */
void Square::translate(double* v){
    vadd(pos,pos,v);
    tris[0]->translate(v);
    tris[1]->translate(v);
}

/**
 * Get normal vector
 * @param face      Index of face to calculate normal of
 * @param loc       Position at which to calculate normal
 * @returns         Normal vector (3D)
 */
double* Square::get_normal(int face, double* loc){
    UNUSED(face);
    UNUSED(loc);
    return tris[0]->nrm;
}

/**
 * Re-calculate and store normal vector 
 */
void Square::calc_normals(){
    tris[0]->calc_normals();
}


double* Square::get_coeff(char r, int face_idx, double* loc) {
    return tris[0]->get_coeff(r,face_idx,loc);
}


void Square::set_coeff(char r, double* coeffs, int face_idx, double* loc) {
    tris[0]->set_coeff(r, coeffs,face_idx, loc);
}

void Square::set_color(char r, int hexValue) {
    tris[0]->set_color(r,hexValue);
}

/**
 * Get roughness of entity
 * @param face_idx      Index of face to query
 * @param loc           Position on entity to query
 * @returns             Roughness scalar
 */
 double Square::get_rough(int face_idx, double* loc){
    UNUSED(loc); UNUSED(face_idx);
    return tris[0]->get_rough(0,nullptr);
 }

/**
 * Set roughness of material
 * @param roughness     Roughness value (0 to 1)
 * @param face_idx      Index of face to query
 */
void Square::set_rough(double roughness, int face_idx) {
    UNUSED(face_idx);
    tris[0]->set_rough(roughness,face_idx);
}


/**
 * Rotate this entity around an axis and position by an angle theta
 * @param ax        Rotation axis 
 * @param theta     Angle, in degrees
 * @param cor       Centre of rotation
 */
void Square::rotate(char ax, double theta, double* cor) {

    double corp[3];
    if (cor == nullptr) {
        vequal(corp,pos);
    } else {
        vequal(corp,cor);
    }

    // Rotate postion
    double** mrot = alloc2D(3,3);
    rotmat3D(mrot,ax,theta);
    vsub(pos,pos,corp);
    vmatmul(pos,mrot,pos);
    vadd(pos,pos,corp);
    free2D(mrot,3);

    for (int i = 0; i < 2; i++)
        tris[i]->rotate(ax,theta,corp);

    calc_normals();
}

/**
 * Find where a ray interesects this entity
 * @param ray_src   Point from which ray originates (3D)
 * @param ray_dir   Direction of ray (3D)
 * @param cp_face   Result: Pointer to intersected face (if it does intersect)
 * @param cp_loc    Result: Pointer to location where ray intersects entity (if it does intersect)
 * @returns         Does ray intersect this entity True/False?
 */
bool Square::find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc){

    // Set results to default values
    *cp_face = -1;
    cp_loc[0] = 0.0;
    cp_loc[1] = 0.0;
    cp_loc[2] = 0.0;

    // If hidden return with 'no intersection'
    if (!show)
        return false;

    // Test both triangles
    for (int i = 0; i < 2; i++) {
        if (tris[i]->find_contact(ray_src,ray_dir,cp_face,cp_loc))
            return true;
    }
    return false;
}

