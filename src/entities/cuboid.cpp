#include "../wisteria.hpp"

Cuboid::Cuboid(double x, double y, double z, double w, double h, double d) {
    sqrs[0] = new Square(0,0,0,w,h); sqrs[1] = new Square(0,0,0,w,h); // front and back
    sqrs[2] = new Square(0,0,0,d,h); sqrs[3] = new Square(0,0,0,d,h); // left and right
    sqrs[4] = new Square(0,0,0,w,d); sqrs[5] = new Square(0,0,0,w,d); // top and bottom

    // Move and rotate squares to correct positions
    double c[3] = {0,0,0};
    // back face 
    double v[3] = {0,0,d};
    sqrs[1]->translate(v);        
    // left face
    sqrs[2]->rotate('y',-90,c);
    // right face
    sqrs[3]->rotate('y',-90,c);
    vset(v,0.0); v[0] = w;
    sqrs[3]->translate(v);
    // bottom face
    sqrs[4]->rotate('x',90,c);
    // top face
    sqrs[5]->rotate('x',90,c);
    vset(v,0.0); v[1] = h;
    sqrs[5]->translate(v);


    // Translate cube to correct position
    v[0] = x; v[1] = y; v[2] = z;
    for (int i = 0; i < 6; i++)
        sqrs[i]->translate(v);

    // Update stored position
    pos[0] = x+w/2; pos[1] = y+h/2; pos[2] = z+d/2;
}


Cuboid::~Cuboid() {
    for (int i = 0; i < 6; i++)
        delete sqrs[i];
}

/**
 * Get entity position
 * @returns position vector (3D)
 */
double* Cuboid::get_pos(){
    return pos;
}

/**
 * Is entity volumetric?
 * @returns true/false depending on entity type
 */
bool Cuboid::is_volumetric(){
    return true;
}

/**
 * Return entity type as char
 * @returns Type char
 */
char Cuboid::get_type(){
    return 'c';
}


/**
 * Rotate this entity around an axis and position by an angle theta
 * @param ax        Rotation axis 
 * @param theta     Angle, in degrees
 * @param cor       Centre of rotation
 */
void Cuboid::rotate(char ax, double theta, double* cor) {
    // If no cor supplied, use object position
    double c[3];
    if (cor == nullptr) {
        vequal(c,pos);
    } else {
        vequal(c,cor);
    }

    for (int i = 0; i < 6; i++)
        sqrs[i]->rotate(ax,theta,cor);

    calc_normals();
}


void Cuboid::translate(double* v) {
    vadd(pos,pos,v);
    for (int i = 0; i < 6; i++)
        sqrs[i]->translate(v);
}

void Cuboid::set_coeff(char r, double* coeffs, int face_idx, double* loc) {
    if (face_idx == -1) {
        for (int i = 0; i < 6; i++)
            sqrs[i]->set_coeff(r,coeffs,0,loc);
    } else if (face_idx >=0 && face_idx<6) {
        sqrs[face_idx]->set_coeff(r,coeffs,0,loc);
    }
}

void Cuboid::set_color(char r, int hexValue) {
    for (int i = 0; i < 6; i++)
        sqrs[i]->set_color(r,hexValue);
}


double* Cuboid::get_coeff(char r, int face_idx, double* loc) {
    if (face_idx >=0 || face_idx<6) {
        return sqrs[face_idx]->get_coeff(r,0,loc);
    } else {
        printf(PRT_ERROR"In Cuboid::get_coeff(): face index (%d) out of range \n",face_idx);
        exit(EXIT_FAILURE);
    }
}

/**
 * Get roughness of entity
 * @param face_idx      Index of face to query
 * @param loc           Position on entity to query
 * @returns             Roughness scalar
 */
 double Cuboid::get_rough(int face_idx, double* loc){
    UNUSED(loc);
    if (face_idx > 5) {
        printf(PRT_WARN"In Cuboid::get_rough(), face index out of range. Using 0th face. \n");
        face_idx = 0;
    }
    return sqrs[face_idx]->get_rough(face_idx,nullptr);
 }

/**
 * Set roughness of material
 * @param roughness     Roughness value (0 to 1)
 * @param face_idx      Index of face to query
 */
void Cuboid::set_rough(double roughness, int face_idx) {
    if (face_idx <=-1 || face_idx>=6) {
        printf(PRT_WARN"In Cuboid::set_rough(), face index out of range. Using 0th face. \n");
        face_idx = 0;
    }
    if (face_idx == -1) {
        for (int i = 0; i < 6; i++) {
            sqrs[i]->set_rough(roughness,0);
        }
    } else {
        sqrs[face_idx]->set_rough(roughness,0);
    }
    
}


void Cuboid::calc_normals() {
    for (int i = 0; i < 6; i++)
        sqrs[i]->calc_normals();
}

double* Cuboid::get_normal(int face_idx, double* loc){
    UNUSED(loc);
    if (face_idx >=0 && face_idx<6) {
        return sqrs[face_idx]->get_normal(0,nullptr);
    } else {
        printf(PRT_ERROR"In Cuboid::get_normal(): face index (%d) out of range \n",face_idx);
        exit(EXIT_FAILURE);
    }
}

bool Cuboid::find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc){
    
    // Set initial values
    cp_loc[0] = 0; cp_loc[1] = 0; cp_loc[2] = 0; 
    *cp_face      = -1;
    bool   cp_any = false;
    double cp_min = DBL_BIG;

    // Try each square
    bool    recent;
    int     recent_face;
    double  recent_cp[3];
    double  recent_vec[3];
    double  recent_dist;
    for (int i = 0; i < 6; i++){
        if (!sqrs[i]->show) continue;
        recent = sqrs[i]->find_contact(ray_src,ray_dir,&recent_face,recent_cp);
        if (recent){
            cp_any = true;
            vsub(recent_vec,recent_cp,ray_src);
            vnorm(&recent_dist,recent_vec);
            if (recent_dist < cp_min){
                cp_min = recent_dist;
                vequal(cp_loc,recent_cp);
                *cp_face = i;
            }
        }
    }
    return cp_any;
}

