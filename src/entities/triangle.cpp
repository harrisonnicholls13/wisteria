#include "../wisteria.hpp"

/**
 * Constructor for triangle class
 * @param x         Position (x)
 * @param y         Position (y)
 * @param z         Position (z)
 * @param w         Width (x)
 * @param h         Height (y)
 */
Triangle::Triangle(double x, double y, double z, double w, double h){
    pos[0] = x; pos[1] = y; pos[2] = z;

    // allocate array for vertices
    vxs = alloc2D(3,NDIMS);
    double** m = alloc2D(3,NDIMS); 
    m[0][0]=x;   m[0][1]=y;   m[0][2]=z;
    m[1][0]=x+w; m[1][1]=y;   m[1][2]=z;
    m[2][0]=x;   m[2][1]=y+h; m[2][2]=z;
    
    // allocate array for edges
    egs = alloc2D(3,NDIMS);

    set_vertices(m);
    free2D(m,3);

    // Set default color to black, matte, impermeable, no absorption, no emission
    set_color('d',0x000000);
    set_color('r',0x000000);
    set_color('t',0x000000);
    set_color('a',0x000000);
    set_color('g',0x000000);

}

/**
 * Descructor for triangle class
 */
Triangle::~Triangle(){
    free2D(vxs,3);
    free2D(egs,3);
}

/**
 * Get entity position
 * @returns position vector (3D)
 */
double* Triangle::get_pos(){
    return pos;
}

/**
 * Is entity volumetric?
 * @returns true/false depending on entity type
 */
bool Triangle::is_volumetric(){
    return false;
}

/**
 * Set positions of vertices of triangle
 * @param m         Matrix of shape (3,NDIMS) containing the position of each of the 3 vertices
 */
void Triangle::set_vertices(double** m){
    // Store vertices
    for (int i = 0; i < NDIMS; i++) {
        for (int j = 0; j < NDIMS; j++) {
            vxs[i][j] = m[i][j];
        }
    }

    // Recalculate edge vectors
    vsub(egs[0], vxs[2], vxs[1]);
    vsub(egs[1], vxs[1], vxs[0]);
    vsub(egs[2], vxs[2], vxs[0]);

    // Set position vector
    vequal(pos,vxs[0]);

    // Recalculate normal vector
    calc_normals();
}

/**
 * Re-calculate and store normal vector 
 */
void Triangle::calc_normals(){
    vcross(nrm, egs[1],egs[2]);
    vunit(nrm,nrm);
}

/**
 * Return entity type as char
 * @returns Type char
 */
char Triangle::get_type(){
    return 't';
}

/**
 * Get normal vector
 * @param face      Index of face to calculate normal of
 * @param loc       Position at which to calculate normal
 * @returns         Normal vector (3D)
 */
double* Triangle::get_normal(int face, double* loc){
    UNUSED(face); UNUSED(loc);
    return nrm;
}

/**
 * Translate entity by a vector v
 * @param v         Vector to translate entity by
 */
void Triangle::translate(double* v){
    // For each vertex
    for (int i = 0; i < 3; i++) {
        vadd(vxs[i],vxs[i],v);
    }
    vadd(pos,pos,v);
    // Normal vector should be unchanged
}

/**
 * Rotate this entity around an axis and position by an angle theta
 * @param ax        Rotation axis 
 * @param theta     Angle, in degrees
 * @param cor       Centre of rotation
 */
void Triangle::rotate(char ax, double theta, double* cor) {

        // Make copy of cor, for case when cor=this->pos
        double corp[3];
        if (cor != nullptr) {
            vequal(corp,cor);
        } else {
            vequal(corp,pos);
        }
        

        // Calculate rotation matrix
        double** mrot = alloc2D(3,3);
        rotmat3D(mrot, ax, theta);

        // Make copy of vertices
        double** vnew = alloc2D(3,3);
        for (int i = 0; i < 3; i++) vequal(vnew[i],vxs[i]);

        // Shift to origin, rotate, and shift back 
        vsub(vnew[0],vnew[0],pos);
        vsub(vnew[1],vnew[1],pos);
        vsub(vnew[2],vnew[2],pos);
        vmatmul(vnew[0],mrot,vnew[0]);
        vmatmul(vnew[1],mrot,vnew[1]);
        vmatmul(vnew[2],mrot,vnew[2]);
        vadd(vnew[0],vnew[0],pos);
        vadd(vnew[1],vnew[1],pos);
        vadd(vnew[2],vnew[2],pos);

        // Account for rotation about cor
        double pold[3];
        vequal(pold,pos);
        vsub(pos,pos,corp);
        vmatmul(pos,mrot,pos);
        vadd(pos,pos,corp);

        double diff[3];
        vsub(diff,pos,pold);
        vadd(vnew[0],vnew[0],diff);
        vadd(vnew[1],vnew[1],diff);
        vadd(vnew[2],vnew[2],diff);

        // Save results
        set_vertices(vnew);
        free2D(mrot,3);
        free2D(vnew,3);
}

/**
 * Set ray coefficients for this entity, for a given ray direction
 * @param r             Direct, reflection, or transmission
 * @param coeffs        New ray coefficients (3D)
 * @param face_idx      Which face to set (-1 to set all)
 * @param loc           Location to set coeffs at
 */
void Triangle::set_coeff(char r, double* coeffs, int face_idx, double* loc) {
    UNUSED(face_idx); UNUSED(loc);
    switch (r) {
        case 'd':
            vequal(dire_coeff,coeffs); break;
        case 'r':
            vequal(refl_coeff,coeffs); break;
        case 't':
            vequal(tran_coeff,coeffs); break;
        case 'g':
            vequal(glob_coeff,coeffs); break;
        case 'a':
            vequal(atte_coeff,coeffs); break;
        default:
            printf(PRT_ERROR"In Triangle::set_coeff(), invalid coefficient '%c' \n",r);
            exit(EXIT_FAILURE);
    }
}


/**
 * Return ray coefficients
 * @param r             Ray direction
 * @param face_idx      Index of face to access
 * @param loc           Location to evaluate at 
 * @returns             Pointer to reflectance coefficients
 */
double* Triangle::get_coeff(char r, int face_idx, double* loc) {
    UNUSED(face_idx); UNUSED(loc);
    switch (r) {
        case 'd': return dire_coeff;
        case 'r': return refl_coeff;
        case 't': return tran_coeff;
        case 'g': return glob_coeff;
        case 'a': return atte_coeff;
        default:
            printf(PRT_ERROR"In Triangle::get_coeff(); invalid coefficient '%c' \n",r);
            exit(EXIT_FAILURE);
    }
}

/**
 * Set diffusive reflectance of entity using hexadecmial value - modifies dcoeffs.
 * @param rdir          Ray direction
 * @param hexValue      Hexadecimal color code (e.g. 0xff00ff for magenta)
 */
void Triangle::set_color(char rdir, int hexValue) {
    double r = ((hexValue >> 16) & 0xFF) / 255.0;  // Extract the RR byte
    double g = ((hexValue >> 8) & 0xFF)  / 255.0;  // Extract the GG byte
    double b = ((hexValue) & 0xFF)       / 255.0;  // Extract the BB byte
    double c[3] = {r,g,b};
    set_coeff(rdir,c,-1,nullptr);
}

/**
 * Get roughness of material
 * @param face_idx      Index of face to query
 * @param loc           Position on entity to query
 * @returns             Roughness scalar
 */
 double Triangle::get_rough(int face_idx, double* loc){
    UNUSED(face_idx); UNUSED(loc);
    return rgh;
 }

/**
 * Set roughness of material
 * @param roughness     Roughness value (0 to 1)
 * @param face_idx      Index of face to query
 */
void Triangle::set_rough(double roughness, int face_idx) {
    UNUSED(face_idx);
    roughness = std::min(roughness,1.0);
    roughness = std::max(roughness,0.0);
    rgh = roughness;
}

/**
 * Find where a ray interesects this entity
 * @param ray_src   Point from which ray originates (3D)
 * @param ray_dir   Direction of ray (3D)
 * @param cp_face   Result: Pointer to intersected face (if it does intersect)
 * @param cp_loc    Result: Pointer to location where ray intersects entity (if it does intersect)
 * @returns         Does ray intersect this entity True/False?
 */
bool Triangle::find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc){

    // Set results to default values
    *cp_face = -1;
    cp_loc[0] = 0.0;
    cp_loc[1] = 0.0;
    cp_loc[2] = 0.0;

    // If hidden return with 'no intersection'
    if (!show)
        return false;

    // Find contact point
    // https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
    double ilp_eps = 0.0000001;

    double h[3];
    vcross(h,ray_dir, egs[2]);
    // printf(PRT_MSG"h: %g %g %g \n",h[0],h[1],h[2]);

    double a;
    vdot(&a, egs[1],h);
    // printf(PRT_MSG"egs1: %g %g %g \n",egs[1][0],egs[1][1],egs[1][2]);
    
    if ((a > -ilp_eps) && ( a < ilp_eps)) { // This ray is parallel to this triangle.
        // NOTE: CURRENTLY THIS IS ALWAYS BEING EVALUATED TO TRUE, HENCE THE BROKENNESS
        return false;
    }

    double f = 1.0/a;

    double s[3];
    vsub(s,ray_src, vxs[0]);
    
    double u;
    vdot(&u,s,h);
    u *= f;

    if ((u < 0.0) || ( u > 1.0))
        return false;

    double q[3];
    vcross(q,s,egs[1]);

    double v;
    vdot(&v,ray_dir,q);
    v *= f;
    if ((v < 0.0) || ( (u + v) > 1.0))
        return false;

    double t;
    vdot(&t,egs[2],q);
    t *= f;
    if (t > ilp_eps){
        // Intersects!
        *cp_face = 0;
        cp_loc[0] = ray_src[0] + ray_dir[0]*t;
        cp_loc[1] = ray_src[1] + ray_dir[1]*t;
        cp_loc[2] = ray_src[2] + ray_dir[2]*t;
        return true;
    } else {
        // Does not intersect
        return false;
    }
}

