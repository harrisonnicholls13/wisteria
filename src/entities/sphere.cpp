#include "../wisteria.hpp"

/**
 * Constructor for sphere class
 * @param x         Position (x)
 * @param y         Position (y)
 * @param z         Position (z)
 * @param r         Radius
 */
Sphere::Sphere(double x, double y, double z, double r){
    rad = fabs(r);
    pos[0] = x; pos[1] = y; pos[2] = z;

    set_color('d',0x000000);
    set_color('r',0x000000);
    set_color('t',0x000000);
    set_color('a',0x000000);
    set_color('g',0x000000);
}

/**
 * Destructor for sphere class
 */
Sphere::~Sphere(){
    return;
}

void Sphere::translate(double* v){
    vadd(pos,pos,v);
}

void Sphere::rotate(char ax, double theta, double* cor){

    // If no cor supplied, use object position
    double corp[3];
    if (cor == nullptr) {
        vequal(corp,pos);
    } else {
        vequal(corp,cor);
    }

    double** mrot = alloc2D(3,3);
    rotmat3D(mrot,ax,theta);

    double newpos[3];
    vsub(newpos,pos,corp);
    vmatmul(newpos,mrot,newpos);
    vadd(newpos,newpos,corp);
    vequal(pos,newpos);

    free2D(mrot,3);
}

/**
 * Get entity position
 * @returns position vector (3D)
 */
double* Sphere::get_pos(){
    return pos;
}

/**
 * Is entity volumetric?
 * @returns true/false depending on entity type
 */
bool Sphere::is_volumetric(){
    return true;
}

/**
 * Return entity type as char
 * @returns Type char
 */
char Sphere::get_type(){
    return 'x';
}


/**
 * Re-calculate and store normal vector 
 */
void Sphere::calc_normals(){
    // Not required by sphere class, as each evaluation is different
    return;
}

/**
 * Get normal vector
 * @param face      Index of face to calculate normal of
 * @param loc       Position at which to calculate normal
 * @returns         Normal vector (3D)
 */
double* Sphere::get_normal(int face, double* loc){
    UNUSED(face);
    double n[3] = {0.0};
    vsub(n,loc,pos);
    vunit(nrm,n);
    return nrm;
}


void Sphere::set_coeff(char r, double* coeffs, int face_idx, double* loc) {
    UNUSED(face_idx); UNUSED(loc);
    switch (r) {
        case 'd':
            vequal(dire_coeff,coeffs); break;
        case 'r':
            vequal(refl_coeff,coeffs); break;
        case 't':
            vequal(tran_coeff,coeffs); break;
        case 'g':
            vequal(glob_coeff,coeffs); break;
        case 'a':
            vequal(atte_coeff,coeffs); break;
        default:
            printf(PRT_ERROR"In Sphere::set_coeff(), invalid coefficient '%c' \n",r);
            exit(EXIT_FAILURE);
    }
}


void Sphere::set_color(char rdir, int hexValue) {
    double r = ((hexValue >> 16) & 0xFF) / 255.0;  // Extract the RR byte
    double g = ((hexValue >> 8) & 0xFF) / 255.0;   // Extract the GG byte
    double b = ((hexValue) & 0xFF) / 255.0;        // Extract the BB byte
    double c[3] = {r,g,b};
    set_coeff(rdir,c,-1,nullptr);
}

/**
 * Return reflectance coefficients
 * @returns     Pointer to reflectance coefficients
 */
double* Sphere::get_coeff(char r, int face_idx, double* loc) {
    UNUSED(face_idx); UNUSED(loc);
    switch (r) {
        case 'd': return dire_coeff;
        case 'r': return refl_coeff;
        case 't': return tran_coeff;
        case 'g': return glob_coeff;
        case 'a': return atte_coeff;
        default:
            printf(PRT_ERROR"In Sphere::get_coeff(); invalid coefficient '%c' \n",r);
            exit(EXIT_FAILURE);
    }
}

/**
 * Get roughness of entity
 * @param face_idx      Index of face to query
 * @param loc           Position on entity to query
 * @returns             Roughness scalar
 */
 double Sphere::get_rough(int face_idx, double* loc){
    UNUSED(face_idx); 
    // Assume isotropic for now.
    // Maybe implement other distributions later.
    UNUSED(loc);
    return rgh;
 }


/**
 * Set roughness of material
 * @param roughness     Roughness value (0 to 1)
 * @param face_idx      Index of face to query
 */
void Sphere::set_rough(double roughness, int face_idx) {
    UNUSED(face_idx);
    roughness = std::min(roughness,1.0);
    roughness = std::max(roughness,0.0);
    rgh = roughness;
}

/**
 * Find where a ray interesects this entity
 * @param ray_src   Point from which ray originates (3D)
 * @param ray_dir   Direction of ray (3D)
 * @param cp_face   Result: Pointer to intersected face (if it does intersect)
 * @param cp_loc    Result: Pointer to location where ray intersects entity (if it does intersect)
 * @returns         Does ray intersect this entity True/False?
 */
bool Sphere::find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc){
    //https://en.wikipedia.org/wiki/Line%E2%80%93sphere_intersection

    // Default values
    *cp_face = -1;
    cp_loc[0] = 0.0;
    cp_loc[1] = 0.0;
    cp_loc[2] = 0.0;

    // If hidden
    if (!show) return false;

    // From wikipedia
    double oc[3];    vsub(oc,ray_src,pos);
    double oc_L;     vnorm(&oc_L,oc);
    double u[3];     vunit(u,ray_dir);
    double Dd = 0.0; vdot(&Dd,u,oc);
    double D = Dd*Dd - (oc_L*oc_L - rad*rad);


    // No intersection
    if (D <= eps) return false;

    // Test which solution is the correct one
    double d0; vdot(&d0,u,oc); d0 *= -1.0;
    double d1 = d0 + sqrt(D);
    double d2 = d0 - sqrt(D);

    // Calc intersection points
    double u1[3] = {0.0}; 
    double p1[3] = {0.0}; vscamul(u1,u,d1); vadd(p1,ray_src,u1);
    double u2[3] = {0.0}; 
    double p2[3] = {0.0}; vscamul(u2,u,d2); vadd(p2,ray_src,u2);

    // Determine best point
    double D1[3] = {0.0}; vsub(D1,p1,ray_src); 
    double D1_norm = 0.0; vnorm(&D1_norm,D1);
    double D2[3] = {0.0}; vsub(D2,p2,ray_src);
    double D2_norm = 0.0; vnorm(&D2_norm,D2);

    // Check if either of them are too close to the ray_src
    // If neither are too close, select best point
    int bcase = 0;
    if (d1 < d2){
        vequal(cp_loc,p1);
        bcase = 1;
    } else {
        vequal(cp_loc,p2);
        bcase = 2;
    }

    // if ( (bcase == 1) && (D1_norm < 1E-10)) {
    //     vequal(cp_loc,p2);
    // }
    // if ( (bcase == 2) && (D2_norm < 1E-10)) {
    //     vequal(cp_loc,p1);
    // }


    *cp_face = 0;
    return true;
}


