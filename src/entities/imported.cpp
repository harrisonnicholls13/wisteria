#include "../wisteria.hpp"


Imported::Imported(std::string filepath, double scale) {

    // Read .obj file contents
    //https://en.wikipedia.org/wiki/Wavefront_.obj_file
    std::ifstream input(filepath);
    std::vector<std::string> lines;
    std::string readline;
    if (input.is_open()) {
        // printf(PRT_MSG"Loading Imported entity '%s'... \n",filepath.c_str());
        for (std::string line; getline( input, readline ); )
            lines.push_back(readline);
        input.close();
    } else {
        printf(PRT_ERROR"In Imported(): Cannot initialise imported entity from file '%s' \n",filepath.c_str());
        exit(EXIT_FAILURE);
    }


    // Variables for parsing data
    std::string pfx;
    std::string parseline;
    int nls = (int) lines.size(); // number of lines

    // Count number of faces/verts, so we know how much memory to allocate
    for (int i = 0; i < nls; i++){
        parseline = lines.at(i);
        pfx = parseline.substr(0, 2);

        // store number of vertices and normals
        if (pfx == "v ") nvs++; 
        if (pfx == "vn") nns++; 

        // store number of faces/tris
        if (pfx == "f ") {
            int tpf = 1;
            int num_sls = 0;
            for (int j = 0; j < (int) parseline.size(); j++) {
                if (parseline.at(j) == '/') num_sls++;
            }
            if (num_sls == 8) tpf = 2;
            nts += tpf;
            nfs += 1;
        }
    }


    // Store vertex data
    double** vxs = alloc2D(nvs,3);
    int vxc = 0;        // counter for which vertex we are reading in
    for (int i = 0; i < nls; i++){
        parseline = lines.at(i);
        pfx = parseline.substr(0, 2);
        if (pfx == "v "){
            sscanf(parseline.c_str(), "v %lf %lf %lf \n", &vxs[vxc][0], &vxs[vxc][1], &vxs[vxc][2]);
            vscamul(vxs[vxc],vxs[vxc],scale);
            vxc++;
        }
    }

    // Store normal data
    double** nms = alloc2D(nns,3);
    int nmc = 0;        // counter for which normal we are reading in
    for (int i = 0; i < nls; i++){
        parseline = lines.at(i);
        pfx = parseline.substr(0, 2);
        if (pfx == "vn"){
            sscanf(parseline.c_str(), "vn %lf %lf %lf \n", &nms[nmc][0], &nms[nmc][1], &nms[nmc][2]);
            nmc++;
        }
    }

    
    // Store data relating faces to vertices and normals
    int** fvs = new int*[nfs];    // for each face, which vertices it needs
    for (int i = 0; i < nfs; ++i) // allocate memory for this storage
        fvs[i] = new int[4];
    int** fns = new int*[nfs];    // for each face, which normals it needs
    for (int i = 0; i < nfs; ++i) // allocate memory for this storage
        fns[i] = new int[4];

    int fcc = -1; // counter for which face we are reading in
    int d;        // dummy variable for absorbing output from sscanf
    int tpf = 0;  // triangles for current face
    int num_sls = 0;    // Number of '/' chars in line
    for (int i = 0; i < nls; i++){ // loop over lines

        // Work out how many triangles this face has, again
        parseline = lines.at(i);
        tpf = 0;
        num_sls = 0;
        for (int j = 0; j < (int) parseline.size(); j++) {
            if (parseline.at(j) == '/') num_sls++;
        }
        if (num_sls == 8) tpf = 2;
        if (num_sls == 6) tpf = 1;
        if (tpf > 0)      fcc++;

        // store vertex index data based on this
        if (tpf == 1) {
            sscanf(parseline.c_str(),"f %d/%d/%d %d/%d/%d %d/%d/%d \n",&fvs[fcc][0],&d,&fns[fcc][0],
                                                                       &fvs[fcc][1],&d,&fns[fcc][1],
                                                                       &fvs[fcc][2],&d,&fns[fcc][2]);
            fvs[fcc][0] -= 1; // subtract one, because the file indexes from 1
            fvs[fcc][1] -= 1;
            fvs[fcc][2] -= 1;
            fvs[fcc][3] = -1; // don't need to store a 4th vertex; used as a flag for how many tris are in this face
            fns[fcc][0] -= 1; // and for normals
            fns[fcc][1] -= 1;
            fns[fcc][2] -= 1;
            fns[fcc][3] = -1;
        } else if (tpf == 2) {
            sscanf(parseline.c_str(),"f %d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d \n",&fvs[fcc][0],&d,&fns[fcc][0],
                                                                                &fvs[fcc][1],&d,&fns[fcc][1],
                                                                                &fvs[fcc][2],&d,&fns[fcc][2],
                                                                                &fvs[fcc][3],&d,&fns[fcc][3]);
            fvs[fcc][0] -= 1; // as above
            fvs[fcc][1] -= 1;
            fvs[fcc][2] -= 1;
            fvs[fcc][3] -= 1;
            fns[fcc][0] -= 1; // and for normals
            fns[fcc][1] -= 1;
            fns[fcc][2] -= 1;
            fns[fcc][3] -= 1;
        }
    }

    // For each face, make triangles as required
    tris = new Triangle*[nts];
    Triangle* new_tri;
    int trc = 0;                   // triangle counter
    double** vmat = alloc2D(3,3);  // matrix for vertices
    for (int i = 0; i < nfs; i++){ // loop over faces

        // create 1st triangle
        new_tri = new Triangle(0,0,0,1,1);
        vequal(vmat[0],vxs[fvs[i][0]]);
        vequal(vmat[1],vxs[fvs[i][1]]);
        vequal(vmat[2],vxs[fvs[i][2]]);
        new_tri->set_vertices(vmat);
        vequal(new_tri->nrm,nms[fns[i][0]]);
        tris[trc] = new_tri;
        trc++;

        // create 2nd triangle, if required
        if (fvs[i][3] != -1) {
            new_tri = new Triangle(0,0,0,1,1);
            vequal(vmat[0],vxs[fvs[i][0]]);
            vequal(vmat[1],vxs[fvs[i][2]]);
            vequal(vmat[2],vxs[fvs[i][3]]);
            new_tri->set_vertices(vmat);
            vequal(new_tri->nrm,nms[fns[i][0]]);
            tris[trc] = new_tri;
            trc++;
        }

    }

    // Calculate position of this object as the average of all triangle positions
    double pcum[3] = {0.0}; // cumulative position
    for (int i = 0; i < nts; i++) {
        vadd(pcum,pcum,tris[i]->get_pos());
    }
    vscamul(pcum,pcum,1.0/nts);
    vequal(pos,pcum);

    // Free memory
    free2D(vmat,3);
    free2D(vxs,nvs);
    free2D(nms,nns);
    for (int i = 0; i < nfs; i++) delete[] fvs[i];
    delete[] fvs;
    for (int i = 0; i < nfs; i++) delete[] fns[i];
    delete[] fns;

}


Imported::~Imported(){
    for (int i = 0; i < nts; i++)
        delete tris[i];
    delete[] tris;
}

/**
 * Get entity position
 * @returns position vector (3D)
 */
double* Imported::get_pos(){
    return pos;
}

/**
 * Is entity volumetric?
 * @returns true/false depending on entity type
 */
bool Imported::is_volumetric(){
    return true;
}

/**
 * Return entity type as char
 * @returns Type char
 */
char Imported::get_type(){
    return 'i';
}


void Imported::rotate(char ax, double theta, double* cor) {
    double c[3] = {0.0};
    if (cor == nullptr) {
        vequal(c,pos);
    } else {
        vequal(c,cor);
    }

    for (int i = 0; i < nts; i++)
        tris[i]->rotate(ax,theta,c);

    // Calculate position of this object as the average of all triangle positions
    double pcum[3] = {0.0}; // cumulative position
    for (int i = 0; i < nts; i++) {
        vadd(pcum,pcum,tris[i]->get_pos());
    }
    vscamul(pcum,pcum,1.0/nts);
    vequal(pos,pcum);
    
}


void Imported::translate(double* v) {
    for (int i = 0; i < nts; i++)
        tris[i]->translate(v);
}


void Imported::set_coeff(char r, double* dcoeffs, int face, double* loc) {
    if (face < 0 || face>nts) {
        printf(PRT_ERROR"In Imported::set_coeff(), face index out of range \n");
        exit(EXIT_FAILURE);
    }
    tris[face]->set_coeff(r,dcoeffs,0,loc);
}


double* Imported::get_coeff(char r, int face, double* loc) {
    if (face<0 || face>nts) {
        printf(PRT_ERROR"In Imported::get_coeff(), face index out of range \n");
        exit(EXIT_FAILURE);
    }
    return tris[face]->get_coeff(r,0,loc);
}


void Imported::set_color(char r, int hexValue) {
    for (int i = 0; i < nts; i++)
        tris[i]->set_color(r,hexValue);
}

/**
 * Get roughness of entity
 * @param face_idx      Index of face to query
 * @param loc           Position on entity to query
 * @returns             Roughness scalar
 */
 double Imported::get_rough(int face_idx, double* loc){
    UNUSED(loc);
    if (face_idx >= nts) {
        printf(PRT_WARN"In Imported::get_rough(), face index (%d) out of range. \n",face_idx);
        exit(EXIT_FAILURE);
    }
    return tris[face_idx]->get_rough(0,nullptr);
 }

 /**
 * Set roughness of material
 * @param roughness     Roughness value (0 to 1)
 * @param face_idx      Index of face to query
 */
void Imported::set_rough(double roughness, int face_idx) {
    // Check face_idx is valid
    if (face_idx <=-1 || face_idx>=nts) {
        printf(PRT_WARN"In Imported::set_rough(), face index out of range. Using 0th face. \n");
        face_idx = 0;
    }

    if (face_idx == -1) {
        // Do all faces
        for (int i = 0; i < nts; i++) 
            tris[i]->set_rough(roughness,0);
    } else {
        // Do only one face
        tris[face_idx]->set_rough(roughness,0);
    }
}


void Imported::calc_normals() {
    for (int i = 0; i < nts; i++)
        tris[i]->calc_normals();
}

double* Imported::get_normal(int face, double* loc) {
    UNUSED(loc);
    if (face<0 || face>nts) {
        printf(PRT_ERROR"In Imported::get_normal(), face index %d out of range \n",face);
        exit(EXIT_FAILURE);
    }
    return tris[face]->get_normal(0,nullptr);
}
 
bool Imported::find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc) {
    // Code adapted from Cuboid::find_contact()
    // Set initial values
    cp_loc[0] = 0; cp_loc[1] = 0; cp_loc[2] = 0; 
    *cp_face      = -1;
    bool   cp_any = false;
    double cp_min = DBL_BIG;

    // Try each square
    bool    recent;
    int     recent_face;
    double  recent_cp[3];
    double  recent_vec[3];
    double  recent_dist;
    for (int i = 0; i < nts; i++){
        if (!tris[i]->show) continue;
        recent = tris[i]->find_contact(ray_src,ray_dir,&recent_face,recent_cp);
        if (recent){
            cp_any = true;
            vsub(recent_vec,recent_cp,ray_src);
            vnorm(&recent_dist,recent_vec);
            if (recent_dist < cp_min){
                cp_min = recent_dist;
                vequal(cp_loc,recent_cp);
                *cp_face = i;
            }
        }
    }
    return cp_any;
}



