#include "../wisteria.hpp"

// Type chars:
//  t = Triangle
//  s = Square
//  x = True sphere
//  c = Cuboid
//  i = Imported entity

// Prototype functions
void calc_new_coeff(char r, double* coeffs, double* dire_coeff,  double* refl_coeff,  double* tran_coeff);

// Entity objects...
class Entity{

    // Variables
    protected:
        double      pos[3] =        {0.0};   // Formal 'position' of the whole entity
        double      dire_coeff[3] = {1.0};   // Direct illumination coeffs |
        double      refl_coeff[3] = {0.0};   // Reflection coeffs          |
        double      tran_coeff[3] = {0.0};   // Transmission coeffs        |- Normalised 
        double      glob_coeff[3] = {0.0};   // Global illumination coeffs | 
        double      atte_coeff[3] = {0.0};   // Attenuation coeffs         |
        double      rgh    =        0.0;     // roughness (0 to 1)
    public:
        double      amult =         1.0;     // Attenuation multiplier
        double      rfractn =       1.0;     // refractive index
        bool        show   = true;
        std::string name   = "unnamed";

    // Functions
    public:
        // To be overriden by derived classes
        virtual         ~Entity() = default;
        virtual bool    is_volumetric() = 0;
        virtual char    get_type() = 0;
        virtual void    set_color(char r, int hexValue) = 0;
        virtual void    set_coeff(char r, double* coeffs, int face_idx, double* loc) = 0;
        virtual double* get_coeff(char r, int face_idx, double* loc) = 0;
        virtual void    set_rough(double rgh, int face_idx) = 0;
        virtual double  get_rough(int face_idx, double* loc) = 0;
        virtual void    rotate(char ax, double theta, double* cor) = 0;
        virtual void    translate(double* v) = 0;
        virtual void    calc_normals() = 0;
        virtual double* get_normal(int face, double* loc) = 0;
        virtual bool    find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc) = 0;
};

class Triangle : public Entity{
    
    // Variables
    public:
        double**    vxs; // vertices
        double**    egs; // edges
        double      nrm[3] = {0.0};
    
    // Functions
    public:
                Triangle(double x, double y, double z, double w, double h);
                ~Triangle();
        double* get_pos();
        void    set_vertices(double** m);
        void    rotate(char ax, double theta, double* cor);
        void    translate(double* v);

        bool    is_volumetric();
        char    get_type();
        void    set_rough(double rgh, int face_idx);
        double  get_rough(int face_idx, double* loc);
        void    set_color(char r, int hexValue);
        void    set_coeff(char r, double* coeffs, int face_idx, double* loc);
        double* get_coeff(char r, int face_idx, double* loc);

        void    calc_normals();
        double* get_normal(int face, double* loc);
        bool    find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc);
        
};

class Square : public Entity{
    
    // Variables
    public:
        Triangle*   tris[2]; // tris
    private:
        double      olp = 1e-4; // overlap

    // Functions
    public:
                Square(double x, double y, double z, double w, double h);
                ~Square();
        double* get_pos();
        void    rotate(char ax, double theta, double* cor);
        void    translate(double* v);

        bool    is_volumetric();
        char    get_type();
        void    set_rough(double rgh, int face_idx);
        double  get_rough(int face_idx, double* loc);
        void    set_color(char r, int hexValue);
        void    set_coeff(char r, double* coeffs, int face_idx, double* loc);
        double* get_coeff(char r, int face_idx, double* loc);

        void    calc_normals();
        double* get_normal(int face, double* loc);
        bool    find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc);
        
};


class Sphere : public Entity{
    
    // Variables
    public:
        double      rad;
    private:
        double      nrm[3] = {0.0};
        double      eps = 1e-10; // error on intersection
    
    // Functions
    public:
                Sphere(double x, double y, double z, double r);
                ~Sphere();
        double* get_pos();
        void    rotate(char ax, double theta, double* cor);
        void    translate(double* v);

        bool    is_volumetric();
        char    get_type();
        void    set_rough(double rgh, int face_idx);
        double  get_rough(int face_idx, double* loc);
        void    set_color(char r, int hexValue);
        void    set_coeff(char r, double* coeffs, int face_idx, double* loc);
        double* get_coeff(char r, int face_idx, double* loc);

        void    calc_normals();
        double* get_normal(int face, double* loc);
        bool    find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc);
        
};

class Cuboid : public Entity{
    
    // Variables
    public:
        double      x,y,z,w,h,d; // parameters
        Square*     sqrs[6]; // faces
    
    // Functions
    public:
                Cuboid(double x, double y, double z, double w, double h, double d);
                ~Cuboid();
        double* get_pos();
        void    rotate(char ax, double theta, double* cor);
        void    translate(double* v);

        bool    is_volumetric();
        char    get_type();
        void    set_rough(double rgh, int face_idx);
        double  get_rough(int face_idx, double* loc);
        void    set_color(char r, int hexValue);
        void    set_coeff(char r, double* coeffs, int face_idx, double* loc);
        double* get_coeff(char r, int face_idx, double* loc);

        void    calc_normals();
        double* get_normal(int face, double* loc);
        bool    find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc);
};


class Imported : public Entity{
    
    // Variables
    public:
        Triangle**   tris; // tris
    private:
        int         nfs = 0; // number of faces
        int         nts = 0; // number of tris
        int         nvs = 0; // number of vertices
        int         nns = 0; // number of normals

    // Functions
    public:
                Imported(std::string filepath, double scale);
                ~Imported();
        double* get_pos();
        void    rotate(char ax, double theta, double* cor);
        void    translate(double* v);

        bool    is_volumetric();
        char    get_type();
        void    set_rough(double rgh, int face_idx);
        double  get_rough(int face_idx, double* loc);
        void    set_color(char r, int hexValue);
        void    set_coeff(char r, double* coeffs, int face_idx, double* loc);
        double* get_coeff(char r, int face_idx, double* loc);

        void    calc_normals();
        double* get_normal(int face, double* loc);
        bool    find_contact(double* ray_src, double* ray_dir, int* cp_face, double* cp_loc);
        
};

