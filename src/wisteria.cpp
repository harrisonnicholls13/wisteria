#include "wisteria.hpp"

int main(int argc, char **argv) {

    int exit_code = EXIT_SUCCESS;

    printf(PRT_MSG" __    __  _       _               _         \n");
    printf(PRT_MSG"/ / /\\ \\ \\(_) ___ | |_  ___  _ __ (_)  __ _  \n");
    printf(PRT_MSG"\\ \\/  \\/ /| |/ __|| __|/ _ \\| '__|| | / _` | \n");
    printf(PRT_MSG" \\  /\\  / | |\\__ \\| |_|  __/| |   | || (_| | \n");
    printf(PRT_MSG"  \\/  \\/  |_||___/ \\__|\\___||_|   |_| \\__,_| \n");
    printf(PRT_MSG"A ray tracing code by Harrison Nicholls \n");
    printf(PRT_MSG"\n");
    // ASCII art generated using <https://patorjk.com/software/taag/>

    // Load basis scene
    printf(PRT_MSG"Loading default configuration \n");
    Scene* scene = new Scene("res/default.yaml", false);

    if (scene == nullptr) {
        printf(PRT_ERROR"Could not allocate memory for initial Scene object \n");
        exit(EXIT_FAILURE);
    }

    // Check if provided with script
    int script_result = 0; 
    if (argc == 1) {
        // No script provided; will go to prompt
        printf(PRT_MSG"No script provided \n");
    } else if (argc == 2) {
        // Read script
        script_result = run_script(scene,std::string(argv[1]));
    } else if (argc > 2) {
        // Don't like this
        printf(PRT_WARN"Too many commands; ignoring all of them \n");
        argc = 1;
    }
    

    // Check script result
    if (script_result == 0) {
        // Script is done, but says to go to prompt (or no script was provided)
        if (run_prompt(scene) == 1) {
            // If prompt says to run
            run_engine(scene);
        } 
    } else if (script_result == 1) {
         // Script is done and says to run renderer
        run_engine(scene);
    } else if (script_result == 2) {
        // Script says to exit(good)
        exit_code = EXIT_SUCCESS;
    } else {
        // Script says to exit(bad)
        exit_code = EXIT_FAILURE;
    }
        

    // Tidy and exit
    delete scene;
    printf(PRT_MSG"\n");
    printf(PRT_MSG"Goodbye! \n");
    return exit_code;
}
 