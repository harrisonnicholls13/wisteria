#include "../wisteria.hpp"


int writePPM(Scene* scene, double** data) {
    // https://rosettacode.org/wiki/Bitmap/Write_a_PPM_file#C.2B.2B

    unsigned dimx = (unsigned) scene->cam_rx;
    unsigned dimy = (unsigned) scene->cam_ry;
    
    // Open filestream
    std::ofstream ofs(scene->outfile, std::ios_base::out | std::ios_base::binary);
    ofs << "P6" << std::endl << dimx << ' ' << dimy << std::endl << "255" << std::endl;


    // Write data
    int pix[3] = {0};
    int pin = 0;
    for (int py = 0; py < scene->cam_ry; py++){
        for (int px = scene->cam_rx-1; px >= 0; px--) {
            pin = px*scene->cam_ry + py;
            if (data[pin][3] > 0.0) {
                pix[0] = data[pin][0];
                pix[1] = data[pin][1];
                pix[2] = data[pin][2];
            } else {
                pix[0] = scene->bgCol[0];
                pix[1] = scene->bgCol[1];
                pix[2] = scene->bgCol[2];
            }
            ofs << (char) (pix[0] % 256) << (char) (pix[1] % 256) << (char) (pix[2] % 256);       // red, green, blue
        }
    }
    ofs.close();
    return EXIT_SUCCESS;
}


int writePNG(Scene* scene, double** data) {

    // Code adapted from <http://www.labbookpages.co.uk/software/imgProc/libPNG.html>

    int res = EXIT_SUCCESS;
    FILE *fp = NULL;
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    png_bytep row = NULL;

    // Open file for writing 
    fp = fopen(scene->outfile.c_str(), "wb");
    if (fp == NULL) {
        printf(PRT_ERROR"In writePNG(): Could not open file '%s' for writing\n", scene->outfile.c_str());
        res = EXIT_FAILURE; goto tidy;
    }

    // Initialize write structure
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        printf(PRT_ERROR"In writePNG(): Could not allocate write struct\n");
        res = 1; goto tidy;
    }

    // Initialize info structure
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        printf(PRT_ERROR"In writePNG(): Could not allocate info struct\n");
        res = 1; goto tidy;
    }

    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        printf(PRT_ERROR"In writePNG(): Error during png creation\n");
        res = 1; goto tidy;
    }

    // Image metadata
    png_init_io(png_ptr, fp);
    png_set_IHDR(png_ptr, info_ptr, scene->cam_rx, scene->cam_ry,
            8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
            PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    png_write_info(png_ptr, info_ptr);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    row = (png_bytep) malloc(3 * scene->cam_rx * sizeof(png_byte));

    // Write image data
    int x, y;
    int pin;
    for (y=0 ; y<scene->cam_ry ; y++) {
        for (x = scene->cam_rx-1; x >= 0; x--) {
            pin = x*scene->cam_ry + y;
            if (data[pin][3] > 0.0) {
                row[x*3+0] = data[pin][0];
                row[x*3+1] = data[pin][1];
                row[x*3+2] = data[pin][2];
            } else {
                row[x*3+0] = scene->bgCol[0];
                row[x*3+1] = scene->bgCol[1];
                row[x*3+2] = scene->bgCol[2];
            }
        }
        png_write_row(png_ptr, row);
    }

   // End write
   png_write_end(png_ptr, NULL);


    // Tidy up before exit
    tidy:
        if (fp != NULL) fclose(fp);
        if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
        if (png_ptr != NULL) png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
        if (row != NULL) free(row);

    return res;
}

