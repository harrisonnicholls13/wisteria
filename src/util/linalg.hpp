void vprint(std::string msg, double* vec);
void vnorm(  double* out, double* arr);
void vcross( double* out, double* a, double* b);
void vdot(   double* out, double* a, double* b);
void vunit(  double* out, double* a);
void vsub(   double* out, double* a, double* b);
void vadd(   double* out, double* a, double* b);
void velemul(double* out, double* a, double* b);
void vmatmul(double* out, double** m, double* v);
void vscamul(double* out, double* v, double a);
void vscaadd(double* out, double* v, double a);
void vequal( double* a,   double* b);
void vset(   double* v,   double  a);
void vzero(  double* a);

double*     alloc1D(int w);
void        free1D(double* m);
double**    alloc2D(int w, int h);
void        free2D(double** m, int w);
double***   alloc3D(int w, int h, int d);
void        free3D(double*** m, int w, int h);
double****  alloc4D(int w, int h, int d, int l);
void        free4D(double**** m, int w, int h, int d);
void        rotmat3D(double** out, char ax, double deg);

double      roundTo(double x, int dp);
