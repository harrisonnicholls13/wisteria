#include "../wisteria.hpp"

/**
 * Print vector elements to stdout
 * @param msg       Message to prepend print statement with
 * @param vec       Vector to print
 */
void vprint(std::string msg, double* vec) {
    printf(PRT_MSG"%s %g %g %g \n",msg.c_str(),vec[0],vec[1],vec[2]);
}


/**
 * Calcualate the Euclidean norm of an array
 * @param out       Pointer to output variable
 * @param arr       Vector to calcualate norm of
 */
void vnorm(double* out, double* arr) {
    double sum = 0.0;
    for (int i = 0; i < NDIMS; i++) {
        sum += arr[i] * arr[i];
    }
    *out = sqrt(sum);
}

/**
 * Subtract two vectors (3D), such that out:=a-b
 * @param out       Pointer to output variable
 * @param a         First vector
 * @param b         Second vector
 */
void vsub(double* out, double* a, double* b) {
    for (int i = 0; i < NDIMS; i++) {
        out[i] = a[i] - b[i];
    }
}

/**
 * Add two vectors (3D), such that such that out:=a+b
 * @param out       Pointer to output variable
 * @param a         First vector
 * @param b         Second vector
 */
void vadd(double* out, double* a, double* b) {
    for (int i = 0; i < NDIMS; i++) {
        out[i] = a[i] + b[i];
    }
}


/**
 * Calculate the cross (vector) product of two vectors (3D)
 * @param out       Result
 * @param a         First vector
 * @param b         Second vector
 */
void vcross(double* out, double* a, double* b) {
    double c[3] = {
        a[1]*b[2] - a[2]*b[1],
        a[2]*b[0] - a[0]*b[2],
        a[0]*b[1] - a[1]*b[0]
    };
    vequal(out,c);
}

/**
 * Calculate the dot (scalar) product of two vectors (3D)
 * @param out       Pointer to output variable
 * @param a         First vector
 * @param b         Second vector
 */
void vdot(double* out, double* a, double* b) {
    *out = (a[0]*b[0] + a[1]*b[1] + a[2]*b[2]);
}


/**
 * Calculate the unit vector corresponding to a vector (3D)
 * @param out       Pointer to output variable
 * @param a         Vector to normalise
 */
void vunit(double* out, double* a) {
    double L = 0.0;
    vnorm(&L, a);
    L = 1.0/L; // Invert L, as optimisation.
    out[0] = a[0]*L;
    out[1] = a[1]*L;
    out[2] = a[2]*L;
}


/**
 * Assign values from one vector to another (3D), such that a:=b
 * @param a         Vector to assign to
 * @param b         Vector to assign from
 */
void vequal(double* a, double* b) {
    for (int i = 0; i < 3; i++)
        a[i] = b[i];
}

/**
 * Set all elements of a vector (3D) to a value
 * @param v         Vector to set elements of
 * @param a         Value to assign
 */
void vset(double* v, double a) {
    for (int i = 0; i < 3; i++)
        v[i] = a;
}

/**
 * Set 3D vector to zero
 * @param a         Vector to assign
 */
void vzero(double* a) {
    for (int i = 0; i < 3; i++)
        a[i] = 0.0;
}
 
/**
 * Calculate product of one vector by another, element-wise (3D)
 * @param out       Pointer to output vector
 * @param a         Vector to multiply by
 * @param b         Vector to multiply by
 */
void velemul(double* out, double* a, double* b) {
    for (int i = 0; i < 3; i++)
        out[i] = a[i]*b[i];
}

/**
 * Calculate product of a vector (3D) by a scalar value (uniform)
 * @param out       Pointer to output vector
 * @param v         Vector to multiply
 * @param a         Value to multiply by
 */
void vscamul(double* out, double* v, double a) {
    for (int i = 0; i < 3; i++)
        out[i] = v[i]*a;
}

/**
 * Calculate summation of a vector (3D) by a scalar value (uniform)
 * @param out       Pointer to output vector
 * @param v         Vector to add to
 * @param a         Value to add
 */
void vscaadd(double* out, double* v, double a) {
    for (int i = 0; i < 3; i++)
        out[i] = v[i]+a;
}

/**
 * Calculate product of vector and matrix (3D)
 * @param out       Pointer to output vector
 * @param m         Matrix to multiply by; shape (3,3)
 * @param a         Vector to multiply
 */
void vmatmul(double* out, double** m, double* v) {
    // Backup vector so it is constant
    double b[3] = {v[0],v[1],v[2]};

    // Multiply v by m
    // for (int i = 0; i < 3; i++)
    //     vdot(&out[i],m[i],b);


    out[0] = m[0][0] * b[0] + m[0][1] * b[1] + m[0][2] * b[2];
    out[1] = m[1][0] * b[0] + m[1][1] * b[1] + m[1][2] * b[2];
    out[2] = m[2][0] * b[0] + m[2][1] * b[1] + m[2][2] * b[2];
   
}

/**
 * Allocate 1D array of shape (w)
 * @param w         Width
 * @returns         Pointer to 1D array
 */
double* alloc1D(int w) {
    double* m = new double[w];
    if (m == nullptr) {
        printf(PRT_ERROR"In alloc1D(): Could not allocate memory \n"); exit(EXIT_FAILURE);
    }

    for (int i = 0; i < w; i++){
        m[i] = 0.0;
    }
    return m;
}

/**
 * Free memory allocated to 1D array 
 * @param m         Pointer to 1D array
 */
void free1D(double* m) {
    delete[] m;
}

/**
 * Allocate 2D array of shape (w,h)
 * @param w         Width
 * @param h         Height
 * @returns         Pointer to 2D array
 */
double** alloc2D(int w, int h){
    double** m = new double*[w];
    if (m == nullptr) {
        printf(PRT_ERROR"In alloc2D(): Could not allocate memory \n"); exit(EXIT_FAILURE);
    }

    for (int i = 0; i < w; i++){
        m[i] = new double[h];
        if (m[i] == nullptr) {
            printf(PRT_ERROR"In alloc2D(): Could not allocate memory \n"); exit(EXIT_FAILURE);
        }
        memset(m[i],0,h*sizeof(double)); // Set to zeros
    }
    return m;
}

/**
 * Free memory allocated to 2D array of shape (w,*)
 * @param m         Double pointer to 2D array
 * @param w         Width
 */
void free2D(double** m, int w) {
    for (int i = 0; i < w; i++){
        delete[] m[i];
    }
    delete[] m;
}

/**
 * Allocate 3D array of shape (w,h,d)
 * @param w         Width
 * @param h         Height
 * @param d         Depth
 * @returns         Pointer to 3D array
 */
double*** alloc3D(int w, int h, int d){
    double*** m = new double**[w];
    if (m == nullptr) {
        printf(PRT_ERROR"In alloc3D(): Could not allocate memory \n"); exit(EXIT_FAILURE);
    }

    for (int i = 0; i < w; i++){
        m[i] = alloc2D(h,d);
    }
    return m;
}

/**
 * Free memory allocated to 3D array of shape (w,h,*)
 * @param m         Double pointer to 3D array
 * @param w         Width
 * @param h         Height
 */
void free3D(double*** m, int w, int h) {
    for (int i = 0; i < w; i++){
        free2D(m[i],h);
    }
    delete[] m;
}

/**
 * Allocate 3D array of shape (w,h,d,l)
 * @param w         Width
 * @param h         Height
 * @param d         Depth
 * @param l         Length
 * @returns         Pointer to 4D array
 */
double**** alloc4D(int w, int h, int d, int l){
    double**** m = new double***[w];
    if (m == nullptr) {
        printf(PRT_ERROR"In alloc4D(): Could not allocate memory \n"); exit(EXIT_FAILURE);
    }

    for (int i = 0; i < w; i++){
        m[i] = alloc3D(h,d,l);
    }
    return m;
}

/**
 * Free memory allocated to 4D array of shape (w,h,d,*)
 * @param m         Double pointer to 3D array
 * @param w         Width
 * @param h         Height
 * @param d         Depth
 */
void free4D(double**** m, int w, int h, int d) {
    for (int i = 0; i < w; i++){
        free3D(m[i],h,d);
    }
    delete[] m;
}

/**
 * Evaluate the 3D rotation matrix for a given axis and angle
 * @param out       Pointer to output variable
 * @param ax        Rotation axis
 * @param deg       Rotation angle (degrees)
 */
void rotmat3D(double** out, char ax, double deg){
    // https://en.wikipedia.org/wiki/Rotation_matrix#In_three_dimensions

    double a = deg * (M_PI/180.0); // Convert to radians
    double cosa = cos(a);
    double sina = sin(a);

    // Set to identity matrix
    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++) {
            if (i == j) {
                out[i][j] = 1.0;
            } else {
                out[i][j] = 0.0;
            }
        }
    }

    // Set elements of rotation matrix for this axis
    if (ax == 'z'){
        out[0][0]=cosa;  out[0][1]=-sina;
        out[1][0]=sina;  out[1][1]=cosa; 
    } else if (ax == 'x') {
        out[1][1]=cosa;  out[1][2]=-sina;
        out[2][1]=sina;  out[2][2]=cosa;
    } else if (ax == 'y') {
        out[0][0]=cosa;  out[0][2]=sina;
        out[2][0]=-sina; out[2][2]=cosa;
    } else {
        printf(PRT_ERROR"In rotmat3D(): Invalid rotation axis '%c' \n",ax);
        exit(EXIT_FAILURE);
    }

}

/**
 * Round a number to a so many decimal places
 * @param x     Number to round
 * @param dp    Number of decimal places to use
 * @returns     Rounded number
 */
double roundTo(double x, int dp) {
    if (dp < 1) {
        return round(x);
    } else {
        double sf = 1.0;
        for (int i = 0; i < dp; i++) sf *= 10;
        return (round( x * sf ) / sf);
    }
}
