#include <stdio.h>
#include <cmath>
#include <cstring>
#include <cerrno>
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <fstream>
#include <cstdio>
#include <stdint.h>
#include <random>
#include <iomanip>
#include <map>
#include <png.h>
#include "yaml-cpp/yaml.h"

// Prefixes to print messages (with colors!)
#define PRT_SPACE		    "------------------------"
#define PRT_MSG			    "\033[0;32m[Message]  \033[0m"  // General message
#define PRT_PRMPT		    "\033[0;36m[Command]  \033[0m"  // Prompt
#define PRT_QUERY		    "\033[0;34m[ Query ]  \033[0m"  // Query for input
#define PRT_WARN		    "\033[0;33m[Warning]  \033[0m"  // Warning 
#define PRT_ERROR		    "\033[0;31m[ Error ]  \033[0m"  // Error

// Macros for neater code
#define UNUSED(_x)                ( (void)(_x) )
#define EVEN(_xv)                 ( ((_xv) % 2) == 0 ? 1 : 0)        // Check if x is even
#define VTOA(_vec)                (&_vec[0])  // convert vector to array
#define DBL_PCT_CNG(_fnl,_ini)    ( fabs( ((_fnl) - (_ini)) / (_ini) * 100 ))   // Calculate percentage change in a value (for doubles)

// Constants and units
#define DBL_BIG     (1e300)         // Big value for a double
#define DBL_SML     (1e-300)        // Small value for a double
#define NDIMS       (3)             // Number of dimensions 
#define NAME_LEN    (16)            // Max char length of object names
#define DEBUG       (0)             // Enable debug features

// Define guard
#ifndef GUARD
#define GUARD

#include "util/linalg.hpp"
#include "lights/lights.hpp"
#include "entities/entities.hpp"
#include "core/scene.hpp"
#include "util/image.hpp"
#include "core/engine.hpp"
#include "core/prompt.hpp"

#endif
