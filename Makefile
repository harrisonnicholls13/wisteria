# Makefile for project. Adapted from: https://stackoverflow.com/a/48268254

CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -g 
LDFLAGS = -lyaml-cpp -lpng

OBJDIR = bin
SRCDIR = src

SRC := $(shell find $(SRCDIR) -name "*.cpp")
OBJ := $(SRC:%.cpp=$(OBJDIR)/%.o)

INCLUDES = -I ./inc -I /usr/local/include  -I /usr/include/ -L /usr/lib64/ -L /usr/lib
INCLUDES := $(shell find src/ -mindepth 1 -maxdepth 1 -type d -printf '-I ./src/%f\n')

APP = run/wisteria

all: $(APP)

$(APP): $(OBJ)
	@echo "== LINKING EXECUTABLE $(APP)"
	@$(CXX) $^ $(LDFLAGS) $(INCLUDES) -o $(APP)

$(OBJDIR)/%.o: %.cpp
		@echo "COMPILING SOURCE $< INTO OBJECT $@"
		@mkdir -p '$(@D)'
		@$(CXX) -c $(CXXFLAGS) $(INCLUDES) $< -o $@
		
clean:
	find . -name *.o -delete
	rm -f $(APP)
	
	
