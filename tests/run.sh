#!/bin/bash
# To be run from the root Wisteria directory

# Default status = ok
status=0

# Run the code 
./run/wisteria tests/quick.wis > /dev/null

# Check output
if [ $? -eq 0 ]
then
  echo "Success"
else
  echo "Failure"
  status=1
fi

exit $status
