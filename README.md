# Wisteria 
A 3D ray tracing engine by Harrison Nicholls 


## System requirements
* make
* [yaml-cpp](https://github.com/jbeder/yaml-cpp)
* [libpng](http://www.libpng.org/pub/png/libpng.html)
* g++
* At least 4 virtual cores
* GNU/Linux (but will probably work on MacOS)

## Compiling the code
Run `make` inside the main directory, which will generate an executable file `run/wisteria`.


## Running the code
Run the executable file `run/wisteria`. The program has a prompt interface for interacting with it.
The program is capable of parsing script files, which run commands compatible with the prompt, including commands to load static configuration files.
Additionally, there is a Python script `run/animate.py` which can run the executable repeatedly, and composite the results into a video.


## Overview
* 3D ray-tracing code written in C++.
* Can customise entities and lights within the scene by creating configuration files, entering commands, and running scripts of commands. 
* Render settings are also configurable via commands, scripts, and configuration files.
* Supports monte-carlo ray tracing with reflection and transmission calculations, with lambertian illumination.
* The ray-tracing engine spawns multiple processes, so that rays can be calculated in parallel in 'chunks'.
* Has the capability to load Wavefront `.obj` files from the disk.
* Aims to support antialiasing, refraction, animation, image textures, normal maps, physics simulations, scripting, and volumentic rendering.
* Generates images files in `.png` or `.ppm` image formats.

## Example results
<img src="doc/readme_imgs/example1.png" alt="Example 1" style="height: 400px; width:640px;"/> <br />     
<img src="doc/readme_imgs/example2.png" alt="Example 2" style="height: 400px; width:640px;"/>       

## License
This work is available under the BSD 3-Clause Clear license

