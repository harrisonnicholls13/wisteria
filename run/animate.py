#!/usr/bin/env python3

print("Start")

import yaml
import sys
import numpy as np
import subprocess
import imageio
import glob
import os
import progressbar

# params
run =   "run/wisteria"
args1 =  np.linspace(22,-18,80)
prm1  =  "camera_posX"
args2 =  np.linspace(22,-18,80)
prm2  =  "camera_long"

# prepare code
if ( len (sys.argv) == 1):
    yamlbase = "res/world.yaml"
else:
    yamlbase = sys.argv[1]
files = glob.glob('res/frames/*')
for f in files:
    os.remove(f)
yamlanim = "res/frames/anim.yaml"
files = []

# Run for each arg value
bar = progressbar.ProgressBar(max_value=len(args1))
for i in range(len(args1)):

    with open(yamlbase) as f:
        yaml_read = yaml.safe_load(f)
    
    fnew = "res/frames/frame%d.png"%i
    files.append(fnew)
    
    yaml_read["outfile"] = fnew        
    yaml_read[prm1] = float(args1[i])
    yaml_read[prm2] = float(args2[i])

    with open(yamlanim, "w") as f:
        yaml.dump(yaml_read, f)
        
    subprocess.run([run, yamlanim],stdout=subprocess.DEVNULL) 
    bar.update(i+1)
    
# convert frames to video
print("Converting frames...")
subprocess.run(["ffmpeg", "-y", "-i", "res/frames/frame%d.png", "-c:v", "libx264", "-framerate", "25", "-loglevel", "quiet", "-pix_fmt", "yuv420p", "res/anim.mp4"],stdout=subprocess.DEVNULL)
    
print("Done!")

